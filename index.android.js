import "./app/config/ReactotronConfig";
import React from "react";
import {AppRegistry} from "react-native";
import Root from "./app/modules/base/containers/Root";

AppRegistry.registerComponent('nextquestion', () => Root);
