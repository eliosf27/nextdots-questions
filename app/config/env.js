export default {
    "version": "0.2.1",
    "env": "development",
    "api": {
        "url": "http://nextdots.com/",
    },
    "firebaseConfig": {
        apiKey: "AIzaSyAnYdke9VDBlejmIQmz2Mw7gZf9GAwomAY",
        authDomain: "kubernetes-155200.firebaseapp.com",
        databaseURL: "https://kubernetes-155200.firebaseio.com",
        storageBucket: "kubernetes-155200.appspot.com",
    },
    "sparkpostApiKey": "95f0e59fb144753089b63f144c0eee3bfa203469",
    "countries": [
        {"country": "Bangladesh", "key": "BD", "code": "880"},
        {"country": "Belgium", "key": "BE", "code": "32"},
        {"country": "Burkina Faso", "key": "BF", "code": "226"},
        {"country": "Bulgaria", "key": "BG", "code": "359"},
        {"country": "Bosnia and Herzegovina", "key": "BA", "code": "387"},
        {"country": "Barbados", "key": "BB", "code": "+1-246"},
        {"country": "Wallis and Futuna", "key": "WF", "code": "681"},
        {"country": "Saint Barthelemy", "key": "BL", "code": "590"},
        {"country": "Bermuda", "key": "BM", "code": "+1-441"},
        {"country": "Brunei", "key": "BN", "code": "673"},
        {"country": "Bolivia", "key": "BO", "code": "591"},
        {"country": "Bahrain", "key": "BH", "code": "973"},
        {"country": "Burundi", "key": "BI", "code": "257"},
        {"country": "Benin", "key": "BJ", "code": "229"},
        {"country": "Bhutan", "key": "BT", "code": "975"},
        {"country": "Jamaica", "key": "JM", "code": "+1-876"},
        {"country": "Bouvet Island", "key": "BV", "code": ""},
        {"country": "Botswana", "key": "BW", "code": "267"},
        {"country": "Samoa", "key": "WS", "code": "685"},
        {"country": "Bonaire, Saint Eustatius and Saba ", "key": "BQ", "code": "599"},
        {"country": "Brazil", "key": "BR", "code": "55"},
        {"country": "Bahamas", "key": "BS", "code": "+1-242"},
        {"country": "Jersey", "key": "JE", "code": "+44-1534"},
        {"country": "Belarus", "key": "BY", "code": "375"},
        {"country": "Belize", "key": "BZ", "code": "501"},
        {"country": "Russia", "key": "RU", "code": "7"},
        {"country": "Rwanda", "key": "RW", "code": "250"},
        {"country": "Serbia", "key": "RS", "code": "381"},
        {"country": "East Timor", "key": "TL", "code": "670"},
        {"country": "Reunion", "key": "RE", "code": "262"},
        {"country": "Turkmenistan", "key": "TM", "code": "993"},
        {"country": "Tajikistan", "key": "TJ", "code": "992"},
        {"country": "Romania", "key": "RO", "code": "40"},
        {"country": "Tokelau", "key": "TK", "code": "690"},
        {"country": "Guinea-Bissau", "key": "GW", "code": "245"},
        {"country": "Guam", "key": "GU", "code": "+1-671"},
        {"country": "Guatemala", "key": "GT", "code": "502"},
        {"country": "South Georgia and the South Sandwich Islands", "key": "GS", "code": ""},
        {"country": "Greece", "key": "GR", "code": "30"},
        {"country": "Equatorial Guinea", "key": "GQ", "code": "240"},
        {"country": "Guadeloupe", "key": "GP", "code": "590"},
        {"country": "Japan", "key": "JP", "code": "81"},
        {"country": "Guyana", "key": "GY", "code": "592"},
        {"country": "Guernsey", "key": "GG", "code": "+44-1481"},
        {"country": "French Guiana", "key": "GF", "code": "594"},
        {"country": "Georgia", "key": "GE", "code": "995"},
        {"country": "Grenada", "key": "GD", "code": "+1-473"},
        {"country": "United Kingdom", "key": "GB", "code": "44"},
        {"country": "Gabon", "key": "GA", "code": "241"},
        {"country": "El Salvador", "key": "SV", "code": "503"},
        {"country": "Guinea", "key": "GN", "code": "224"},
        {"country": "Gambia", "key": "GM", "code": "220"},
        {"country": "Greenland", "key": "GL", "code": "299"},
        {"country": "Gibraltar", "key": "GI", "code": "350"},
        {"country": "Ghana", "key": "GH", "code": "233"},
        {"country": "Oman", "key": "OM", "code": "968"},
        {"country": "Tunisia", "key": "TN", "code": "216"},
        {"country": "Jordan", "key": "JO", "code": "962"},
        {"country": "Croatia", "key": "HR", "code": "385"},
        {"country": "Haiti", "key": "HT", "code": "509"},
        {"country": "Hungary", "key": "HU", "code": "36"},
        {"country": "Hong Kong", "key": "HK", "code": "852"},
        {"country": "Honduras", "key": "HN", "code": "504"},
        {"country": "Heard Island and McDonald Islands", "key": "HM", "code": " "},
        {"country": "Venezuela", "key": "VE", "code": "58"},
        {"country": "Puerto Rico", "key": "PR", "code": "+1-787 and 1-939"},
        {"country": "Palestinian Territory", "key": "PS", "code": "970"},
        {"country": "Palau", "key": "PW", "code": "680"},
        {"country": "Portugal", "key": "PT", "code": "351"},
        {"country": "Svalbard and Jan Mayen", "key": "SJ", "code": "47"},
        {"country": "Paraguay", "key": "PY", "code": "595"},
        {"country": "Iraq", "key": "IQ", "code": "964"},
        {"country": "Panama", "key": "PA", "code": "507"},
        {"country": "French Polynesia", "key": "PF", "code": "689"},
        {"country": "Papua New Guinea", "key": "PG", "code": "675"},
        {"country": "Peru", "key": "PE", "code": "51"},
        {"country": "Pakistan", "key": "PK", "code": "92"},
        {"country": "Philippines", "key": "PH", "code": "63"},
        {"country": "Pitcairn", "key": "PN", "code": "870"},
        {"country": "Poland", "key": "PL", "code": "48"},
        {"country": "Saint Pierre and Miquelon", "key": "PM", "code": "508"},
        {"country": "Zambia", "key": "ZM", "code": "260"},
        {"country": "Western Sahara", "key": "EH", "code": "212"},
        {"country": "Estonia", "key": "EE", "code": "372"},
        {"country": "Egypt", "key": "EG", "code": "20"},
        {"country": "South Africa", "key": "ZA", "code": "27"},
        {"country": "Ecuador", "key": "EC", "code": "593"},
        {"country": "Italy", "key": "IT", "code": "39"},
        {"country": "Vietnam", "key": "VN", "code": "84"},
        {"country": "Solomon Islands", "key": "SB", "code": "677"},
        {"country": "Ethiopia", "key": "ET", "code": "251"},
        {"country": "Somalia", "key": "SO", "code": "252"},
        {"country": "Zimbabwe", "key": "ZW", "code": "263"},
        {"country": "Saudi Arabia", "key": "SA", "code": "966"},
        {"country": "Spain", "key": "ES", "code": "34"},
        {"country": "Eritrea", "key": "ER", "code": "291"},
        {"country": "Montenegro", "key": "ME", "code": "382"},
        {"country": "Moldova", "key": "MD", "code": "373"},
        {"country": "Madagascar", "key": "MG", "code": "261"},
        {"country": "Saint Martin", "key": "MF", "code": "590"},
        {"country": "Morocco", "key": "MA", "code": "212"},
        {"country": "Monaco", "key": "MC", "code": "377"},
        {"country": "Uzbekistan", "key": "UZ", "code": "998"},
        {"country": "Myanmar", "key": "MM", "code": "95"},
        {"country": "Mali", "key": "ML", "code": "223"},
        {"country": "Macao", "key": "MO", "code": "853"},
        {"country": "Mongolia", "key": "MN", "code": "976"},
        {"country": "Marshall Islands", "key": "MH", "code": "692"},
        {"country": "Macedonia", "key": "MK", "code": "389"},
        {"country": "Mauritius", "key": "MU", "code": "230"},
        {"country": "Malta", "key": "MT", "code": "356"},
        {"country": "Malawi", "key": "MW", "code": "265"},
        {"country": "Maldives", "key": "MV", "code": "960"},
        {"country": "Martinique", "key": "MQ", "code": "596"},
        {"country": "Northern Mariana Islands", "key": "MP", "code": "+1-670"},
        {"country": "Montserrat", "key": "MS", "code": "+1-664"},
        {"country": "Mauritania", "key": "MR", "code": "222"},
        {"country": "Isle of Man", "key": "IM", "code": "+44-1624"},
        {"country": "Uganda", "key": "UG", "code": "256"},
        {"country": "Tanzania", "key": "TZ", "code": "255"},
        {"country": "Malaysia", "key": "MY", "code": "60"},
        {"country": "Mexico", "key": "MX", "code": "52"},
        {"country": "Israel", "key": "IL", "code": "972"},
        {"country": "France", "key": "FR", "code": "33"},
        {"country": "British Indian Ocean Territory", "key": "IO", "code": "246"},
        {"country": "Saint Helena", "key": "SH", "code": "290"},
        {"country": "Finland", "key": "FI", "code": "358"},
        {"country": "Fiji", "key": "FJ", "code": "679"},
        {"country": "Falkland Islands", "key": "FK", "code": "500"},
        {"country": "Micronesia", "key": "FM", "code": "691"},
        {"country": "Faroe Islands", "key": "FO", "code": "298"},
        {"country": "Nicaragua", "key": "NI", "code": "505"},
        {"country": "Netherlands", "key": "NL", "code": "31"},
        {"country": "Norway", "key": "NO", "code": "47"},
        {"country": "Namibia", "key": "NA", "code": "264"},
        {"country": "Vanuatu", "key": "VU", "code": "678"},
        {"country": "New Caledonia", "key": "NC", "code": "687"},
        {"country": "Niger", "key": "NE", "code": "227"},
        {"country": "Norfolk Island", "key": "NF", "code": "672"},
        {"country": "Nigeria", "key": "NG", "code": "234"},
        {"country": "New Zealand", "key": "NZ", "code": "64"},
        {"country": "Nauru", "key": "NP", "code": "977"},
        {"country": "Nauru", "key": "NR", "code": "674"},
        {"country": "Niue", "key": "NU", "code": "683"},
        {"country": "Cook Islands", "key": "CK", "code": "682"},
        {"country": "Kosovo", "key": "XK", "code": ""},
        {"country": "Ivory Coast", "key": "CI", "code": "225"},
        {"country": "Switzerland", "key": "CH", "code": "41"},
        {"country": "Colombia", "key": "CO", "code": "57"},
        {"country": "China", "key": "CN", "code": "86"},
        {"country": "Cameroon", "key": "CM", "code": "237"},
        {"country": "Chile", "key": "CL", "code": "56"},
        {"country": "Cocos Islands", "key": "CC", "code": "61"},
        {"country": "Canada", "key": "CA", "code": "1"},
        {"country": "Republic of the Congo", "key": "CG", "code": "242"},
        {"country": "Central African Republic", "key": "CF", "code": "236"},
        {"country": "Democratic Republic of the Congo", "key": "CD", "code": "243"},
        {"country": "Czech Republic", "key": "CZ", "code": "420"},
        {"country": "Cyprus", "key": "CY", "code": "357"},
        {"country": "Christmas Island", "key": "CX", "code": "61"},
        {"country": "Costa Rica", "key": "CR", "code": "506"},
        {"country": "Curacao", "key": "CW", "code": "599"},
        {"country": "Cape Verde", "key": "CV", "code": "238"},
        {"country": "Cuba", "key": "CU", "code": "53"},
        {"country": "Swaziland", "key": "SZ", "code": "268"},
        {"country": "Syria", "key": "SY", "code": "963"},
        {"country": "Sint Maarten", "key": "SX", "code": "599"},
        {"country": "Kyrgyzstan", "key": "KG", "code": "996"},
        {"country": "Kenya", "key": "KE", "code": "254"},
        {"country": "South Sudan", "key": "SS", "code": "211"},
        {"country": "Suriname", "key": "SR", "code": "597"},
        {"country": "Kiribati", "key": "KI", "code": "686"},
        {"country": "Cambodia", "key": "KH", "code": "855"},
        {"country": "Saint Kitts and Nevis", "key": "KN", "code": "+1-869"},
        {"country": "Comoros", "key": "KM", "code": "269"},
        {"country": "Sao Tome and Principe", "key": "ST", "code": "239"},
        {"country": "Slovakia", "key": "SK", "code": "421"},
        {"country": "South Korea", "key": "KR", "code": "82"},
        {"country": "Slovenia", "key": "SI", "code": "386"},
        {"country": "North Korea", "key": "KP", "code": "850"},
        {"country": "Kuwait", "key": "KW", "code": "965"},
        {"country": "Senegal", "key": "SN", "code": "221"},
        {"country": "San Marino", "key": "SM", "code": "378"},
        {"country": "Sierra Leone", "key": "SL", "code": "232"},
        {"country": "Seychelles", "key": "SC", "code": "248"},
        {"country": "Kazakhstan", "key": "KZ", "code": "7"},
        {"country": "Cayman Islands", "key": "KY", "code": "+1-345"},
        {"country": "Singapore", "key": "SG", "code": "65"},
        {"country": "Sweden", "key": "SE", "code": "46"},
        {"country": "Sudan", "key": "SD", "code": "249"},
        {"country": "Dominican Republic", "key": "DO", "code": "+1-809 and 1-829"},
        {"country": "Dominica", "key": "DM", "code": "+1-767"},
        {"country": "Djibouti", "key": "DJ", "code": "253"},
        {"country": "Denmark", "key": "DK", "code": "45"},
        {"country": "British Virgin Islands", "key": "VG", "code": "+1-284"},
        {"country": "Germany", "key": "DE", "code": "49"},
        {"country": "Yemen", "key": "YE", "code": "967"},
        {"country": "Algeria", "key": "DZ", "code": "213"},
        {"country": "United States", "key": "US", "code": "1"},
        {"country": "Uruguay", "key": "UY", "code": "598"},
        {"country": "Mayotte", "key": "YT", "code": "262"},
        {"country": "United States Minor Outlying Islands", "key": "UM", "code": "1"},
        {"country": "Lebanon", "key": "LB", "code": "961"},
        {"country": "Saint Lucia", "key": "LC", "code": "+1-758"},
        {"country": "Laos", "key": "LA", "code": "856"},
        {"country": "Tuvalu", "key": "TV", "code": "688"},
        {"country": "Taiwan", "key": "TW", "code": "886"},
        {"country": "Trinidad and Tobago", "key": "TT", "code": "+1-868"},
        {"country": "Turkey", "key": "TR", "code": "90"},
        {"country": "Sri Lanka", "key": "LK", "code": "94"},
        {"country": "Liechtenstein", "key": "LI", "code": "423"},
        {"country": "Latvia", "key": "LV", "code": "371"},
        {"country": "Tonga", "key": "TO", "code": "676"},
        {"country": "Lithuania", "key": "LT", "code": "370"},
        {"country": "Luxembourg", "key": "LU", "code": "352"},
        {"country": "Liberia", "key": "LR", "code": "231"},
        {"country": "Lesotho", "key": "LS", "code": "266"},
        {"country": "Thailand", "key": "TH", "code": "66"},
        {"country": "French Southern Territories", "key": "TF", "code": ""},
        {"country": "Togo", "key": "TG", "code": "228"},
        {"country": "Chad", "key": "TD", "code": "235"},
        {"country": "Turks and Caicos Islands", "key": "TC", "code": "+1-649"},
        {"country": "Libya", "key": "LY", "code": "218"},
        {"country": "Vatican", "key": "VA", "code": "379"},
        {"country": "Saint Vincent and the Grenadines", "key": "VC", "code": "+1-784"},
        {"country": "United Arab Emirates", "key": "AE", "code": "971"},
        {"country": "Andorra", "key": "AD", "code": "376"},
        {"country": "Antigua and Barbuda", "key": "AG", "code": "+1-268"},
        {"country": "Afghanistan", "key": "AF", "code": "93"},
        {"country": "Anguilla", "key": "AI", "code": "+1-264"},
        {"country": "U.S. Virgin Islands", "key": "VI", "code": "+1-340"},
        {"country": "Iceland", "key": "IS", "code": "354"},
        {"country": "Iran", "key": "IR", "code": "98"},
        {"country": "Armenia", "key": "AM", "code": "374"},
        {"country": "Albania", "key": "AL", "code": "355"},
        {"country": "Angola", "key": "AO", "code": "244"},
        {"country": "Antarctica", "key": "AQ", "code": ""},
        {"country": "American Samoa", "key": "AS", "code": "+1-684"},
        {"country": "Argentina", "key": "AR", "code": "54"},
        {"country": "Australia", "key": "AU", "code": "61"},
        {"country": "Austria", "key": "AT", "code": "43"},
        {"country": "Aruba", "key": "AW", "code": "297"},
        {"country": "India", "key": "IN", "code": "91"},
        {"country": "Aland Islands", "key": "AX", "code": "+358-18"},
        {"country": "Azerbaijan", "key": "AZ", "code": "994"},
        {"country": "Ireland", "key": "IE", "code": "353"},
        {"country": "Indonesia", "key": "ID", "code": "62"},
        {"country": "Ukraine", "key": "UA", "code": "380"},
        {"country": "Qatar", "key": "QA", "code": "974"},
        {"country": "Mozambique", "key": "MZ", "code": "258"}
    ]
}