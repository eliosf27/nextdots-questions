import {GET_USER_FAILURE, GET_USER_REQUEST, GET_USER_SUCCESS} from "./actions";
import FirebaseDB from "../../libs/FirebaseDB";

export const getUserRequest = () => ({type: GET_USER_REQUEST});
export const getUserSuccess = (data) => ({type: GET_USER_SUCCESS, data});
export const getUserFailure = (error) => ({type: GET_USER_FAILURE, error});

export function getUsers() {
    return dispatch => {
        dispatch(getUserRequest());
        this.firebase = new FirebaseDB();
        this.firebase.getData().once("value", data => {
            console.log("data1: ", data)
            console.log("data2: ", data.val())
            if (data.val() === null) {
                dispatch(getUserFailure('No hay clientes registrados'))
            } else {
                let userList = [];
                const users = data.val();
                for (let key in users) {
                    if (users.hasOwnProperty(key)) {
                        let obj = users[key];
                        obj.user["id"] = key;
                        userList.push(obj)
                    }
                }
                dispatch(getUserSuccess(Object.values(users)));
            }
        })
    }
}

const INITIAL_STATE = {
    users: {loading: false, success: false, error: '', data: ''},
};

const baseReducer = (state = INITIAL_STATE, action) => {
    let data;
    switch (action.type) {
        case GET_USER_REQUEST:
            return Object.assign({}, state, {
                users: {loading: true, success: false, error: '', data: []}
            });

        case GET_USER_SUCCESS:
            data = action.data;
            return Object.assign({}, state, {
                users: {loading: false, success: true, error: '', data}
            });

        case GET_USER_FAILURE:
            let error = action.error;
            return Object.assign({}, state, {
                users: {loading: false, success: false, error}
            });

        default:
            return state;
    }
};

export default baseReducer;