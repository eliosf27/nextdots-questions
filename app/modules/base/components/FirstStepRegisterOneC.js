import React, {Component} from "react";
import {Actions} from "react-native-router-flux";
import {
    Body,
    Button,
    Container,
    Content,
    Footer,
    Form,
    Header,
    Icon,
    Input,
    Item,
    Label,
    Left,
    Right,
    StyleProvider,
    Text,
    Title,
    View
} from "native-base";
import {Alert, BackHandler} from "react-native";
import getTheme from "../../../../native-base-theme/components";
import material from "../../../../native-base-theme/variables/material";
import LocalData, {FIRST_STEP_ONE} from "../../../libs/LocalData";
import StringUtils from "../../../libs/StringsUtils";


class FirstStepRegisterOne extends Component {

    constructor(props) {
        super(props);

        this.state = {
            txtName: '',
            txtNameIsInvalid: false,
            txtLastName: '',
            txtLastNameIsInvalid: false,
            txtEmail: '',
            txtEmailIsInvalid: false,
            txtEnterprise: '',
            txtEnterpriseIsInvalid: false,
            txtPosition: '',
            txtPositionIsInvalid: false,

        };
    }

    componentDidMount() {
        this._txtName._root.focus();
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
        LocalData.get(FIRST_STEP_ONE).then(data => {
            if (data !== null) {
                this.setState({
                    txtName: data.name,
                    txtLastName: data.last_name,
                    txtEmail: data.email,
                    txtEnterprise: data.enterprise,
                    txtPosition: data.position,
                });
            }
        });
    }

    onBackPress = () => {
        this.onBack();
        return true;
    };

    onFocusEvt = (event, textState) => {
        if (!this.state[textState]) {
            this.setAttribute(textState, ' ');
        }
    };

    onBack = () => {
        this.clearLocalData();
        Actions.main();
    };

    clearLocalData = () => {
        LocalData.removeAll();
    };

    onNext = () => {
        if (this.state.txtName.trim() === "" || this.state.txtLastName.trim() === "" ||
            this.state.txtEmail.trim() === "" || this.state.txtEnterprise.trim() === "" ||
            this.state.txtPosition.trim() === "") {

            if (this.state.txtName.trim() === "") {
                this.setState({txtNameIsInvalid: true});
            }
            if (this.state.txtLastName.trim() === "") {
                this.setState({txtLastNameIsInvalid: true});
            }
            if (this.state.txtEmail.trim() === "" || !StringUtils.validateEmail(this.state.txtEmail.trim())) {
                this.setState({txtEmailIsInvalid: true});
            }
            if (this.state.txtEnterprise.trim() === "") {
                this.setState({txtEnterpriseIsInvalid: true});
            }
            if (this.state.txtPosition.trim() === "") {
                this.setState({txtPositionIsInvalid: true});
            }

            Alert.alert(
                'Error',
                'Por favor, ingrese datos válidos',
                [
                    {text: 'OK', onPress: () => console.log('OK Pressed!')},
                ]
            )
        } else {
            if (!StringUtils.validateEmail(this.state.txtEmail.trim())) {
                this.setState({txtEmailIsInvalid: true});
                Alert.alert(
                    'Error',
                    'Por favor, ingrese un correo válido',
                    [
                        {text: 'OK', onPress: () => console.log('OK Pressed!')},
                    ]
                )
            } else {
                LocalData.save(FIRST_STEP_ONE, {
                    name: this.state.txtName.trim(),
                    last_name: this.state.txtLastName.trim(),
                    email: this.state.txtEmail.trim(),
                    enterprise: this.state.txtEnterprise.trim(),
                    position: this.state.txtPosition.trim(),
                }).then(obj => {
                    Actions.firstStepRegisterTwo();
                })
            }
        }
    };

    setAttribute = (property, value) => this.setState({[property]: value});

    textInputChange = (event, textState, isInvalid) => {
        const text = event.nativeEvent.text;
        if (text) {
            this.setAttribute(textState, text);
            this.setAttribute(isInvalid, false);
        } else {
            this.setAttribute(textState, text);
            this.setAttribute(isInvalid, true);
        }
    };

    render() {
        return (
            <StyleProvider style={getTheme(material)}>
                <Container>
                    <Header>
                        <Left>
                            <Button transparent onPress={this.onBack}>
                                <Icon name='arrow-back'/>
                            </Button>
                        </Left>
                        <Body>
                        <Title>Registro</Title>
                        </Body>
                        <Right />
                    </Header>
                    <Content>
                        <Form>
                            <Item stackedLabel={this.state.txtName.length > 0}
                                  floatingLabel={!this.state.txtName.length < 0}
                                  style={{marginLeft: 25, marginRight: 25}}
                                  error={this.state.txtNameIsInvalid}>
                                <Label>Nombre</Label>
                                <Input value={this.state.txtName}
                                       returnKeyType={'next'}
                                       ref={c => {
                                           this._txtName = c;
                                       }}
                                       onSubmitEditing={event => {
                                           this._txtLastName._root.focus();
                                       }}
                                       onFocus={event => this.onFocusEvt(event, 'txtName') }
                                       onChange={event => this.textInputChange(event, 'txtName', 'txtNameIsInvalid')}/>
                            </Item>
                            <Item stackedLabel={this.state.txtLastName.length > 0}
                                  floatingLabel={!this.state.txtLastName.length < 0}
                                  style={{marginLeft: 25, marginRight: 25}}
                                  error={this.state.txtLastNameIsInvalid}>
                                <Label>Apellido</Label>
                                <Input value={this.state.txtLastName}
                                       returnKeyType={'next'}
                                       ref={c => {
                                           this._txtLastName = c;
                                       }}
                                       onSubmitEditing={event => {
                                           this._txtEmail._root.focus();
                                       }}
                                       onFocus={event => this.onFocusEvt(event, 'txtLastName') }
                                       onChange={event => this.textInputChange(event, 'txtLastName', 'txtLastNameIsInvalid')}/>
                            </Item>
                            <Item stackedLabel={this.state.txtEmail.length > 0}
                                  floatingLabel={!this.state.txtEmail.length < 0}
                                  style={{marginLeft: 25, marginRight: 25}}
                                  error={this.state.txtEmailIsInvalid}>
                                <Label>Correo electrónico</Label>
                                <Input keyboardType="email-address"
                                       value={this.state.txtEmail}
                                       returnKeyType={'next'}
                                       ref={c => {
                                           this._txtEmail = c;
                                       }}
                                       onSubmitEditing={event => {
                                           this._txtEnterprise._root.focus();
                                       }}
                                       onFocus={event => this.onFocusEvt(event, 'txtEmail') }
                                       onChange={event => this.textInputChange(event, 'txtEmail', 'txtEmailIsInvalid')}/>
                            </Item>
                            <Item stackedLabel={this.state.txtEnterprise.length > 0}
                                  floatingLabel={!this.state.txtEnterprise.length < 0}
                                  style={{marginLeft: 25, marginRight: 25}}
                                  error={this.state.txtEnterpriseIsInvalid}>
                                <Label>Empresa</Label>
                                <Input value={this.state.txtEnterprise}
                                       returnKeyType={'next'}
                                       ref={c => {
                                           this._txtEnterprise = c;
                                       }}
                                       onSubmitEditing={event => {
                                           this._txtPosition._root.focus();
                                       }}
                                       onFocus={event => this.onFocusEvt(event, 'txtEnterprise') }
                                       onChange={event => this.textInputChange(event, 'txtEnterprise', 'txtEnterpriseIsInvalid')}/>
                            </Item>
                            <Item stackedLabel={this.state.txtPosition.length > 0}
                                  floatingLabel={!this.state.txtPosition.length < 0}
                                  style={{marginLeft: 25, marginRight: 25}}
                                  error={this.state.txtPositionIsInvalid}>
                                <Label>Cargo</Label>
                                <Input value={this.state.txtPosition}
                                       returnKeyType={'next'}
                                       ref={c => {
                                           this._txtPosition = c;
                                       }}
                                       onFocus={event => this.onFocusEvt(event, 'txtPosition') }
                                       onChange={event => this.textInputChange(event, 'txtPosition', 'txtPositionIsInvalid')}/>
                            </Item>
                        </Form>
                    </Content>
                    <Footer>
                        <View style={{flex: 1}}/>
                        <Button style={{flex: 3, alignItems: 'center', justifyContent: 'center'}} rounded
                                onPress={this.onNext}>
                            <Text style={{color: '#fff', fontSize: 14}}>SIGUIENTE</Text>
                        </Button>
                        <View style={{flex: 1}}/>
                    </Footer>
                </Container>
            </StyleProvider>
        )
    }
}

export default FirstStepRegisterOne;