import React, {Component} from "react";
import {Alert, StyleSheet, TouchableOpacity} from "react-native";
import {Button, Container, Content, Footer, StyleProvider, Text, View} from "native-base";
import getTheme from "../../../../native-base-theme/components";
import material from "../../../../native-base-theme/variables/material";
import ImagePicker from "react-native-image-picker";
import ResponsiveImage from "react-native-responsive-image";
import {Actions} from "react-native-router-flux";
import LocalData, {ID} from "../../../libs/LocalData";
import FirebaseDB from "../../../libs/FirebaseDB";
import RNFS from "react-native-fs";

const styles = StyleSheet.create({
    containerButton: {
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center',
        marginTop: 100
    },
    containerTitle: {
        fontSize: 27,
        fontWeight: 'bold',
        marginLeft: 20,
        marginTop: 20,
        marginRight: 20
    },
    fixSized: {
        flex: 1
    },
    button: {
        margin: 5
    },
    nextButton: {
        color: '#fff',
    },
    footer: {
        height: 120
    }
});

class FourStep extends Component {

    constructor(props) {
        super(props);
        this.state = {
            path: ''
        };
    }

    addPhoto = () => {
        const options = {
            title: 'Choose an image...',
            takePhotoButtonTitle: 'Camera',
            chooseFromLibraryButtonTitle: 'Gallery',
            cancelButtonTitle: 'Cancel',
            quality: 0.4,
            mediaType: 'photo',
            noData: true
        };

        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
                Alert.alert(
                    'Error',
                    'Ha ocurrido un error al guardar el archivo',
                    [
                        {text: 'OK', onPress: () => console.log('OK Pressed!')},
                    ]
                )
            } else {
                console.log('ImagePicker pick file: ', response);
                this.setState({path: response.path});
            }
        });
    };

    clearLocalData = () => {
        LocalData.removeAll();
    };

    onSave = () => {
        if (this.state.path) {
            RNFS.readFile(this.state.path, "base64").then(result => {
                LocalData.get(ID).then(id => {
                    let firebaseDB = new FirebaseDB();
                    firebaseDB.uploadImage(id, result);
                    Alert.alert(
                        'Éxito',
                        'Imagen guardada exitosamente',
                        [
                            {text: 'OK', onPress: () => console.log('OK Pressed!')},
                        ]
                    );
                    this.clearLocalData();
                    Actions.main();
                });
            });
        } else {
            Alert.alert(
                'Atención',
                'Primero debe cargar una imágen para poder continuar. O presione omitir para terminar.',
                [
                    {text: 'OK', onPress: () => console.log('OK Pressed!')},
                ]
            );
        }
    };

    onNext = () => {
        this.clearLocalData();
        Actions.main();
    };

    render() {
        return (
            <StyleProvider style={getTheme(material)}>
                <Container>
                    <Content>
                        <View>
                            <Text style={{
                                fontSize: 27,
                                fontWeight: 'bold',
                                marginLeft: 20,
                                marginTop: 20,
                                marginRight: 20
                            }}>
                                Escanea tu tarjeta personal
                            </Text>
                            <View style={{
                                alignItems: 'center',
                                flex: 1,
                                justifyContent: 'center',
                                marginTop: 100
                            }}>
                                <TouchableOpacity onPress={this.addPhoto}>
                                    <ResponsiveImage source={require('../../../assets/scan.png')}
                                                     resizeMode="center"
                                                     initWidth="180" initHeight="180"/>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Content>
                    <Footer style={{height: 120}}>
                        <View/>
                        <View style={{flexDirection: 'column', alignItems: 'center', justifyContent: 'center'}}>
                            <View style={{flex: 1}}>
                                <Button rounded warning onPress={this.onSave}>
                                    <Text>Siguiente</Text>
                                </Button>
                            </View>
                            <View style={{flex: 1}}>
                                <Button rounded transparent onPress={this.onNext}>
                                    <Text>Omitir</Text>
                                </Button>
                            </View>
                        </View>
                        <View/>
                    </Footer>
                </Container>
            </StyleProvider>
        )
    }
}

export default FourStep;