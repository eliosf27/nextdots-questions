import React, {Component} from "react";
import {Actions} from "react-native-router-flux";
import {Alert, BackHandler, Picker} from "react-native";
import {
    Body,
    Button,
    Container,
    Content,
    Footer,
    Form,
    Header,
    Icon,
    Input,
    Item,
    Label,
    Left,
    ListItem,
    Right,
    StyleProvider,
    Text,
    Title,
    View
} from "native-base";
import getTheme from "../../../../native-base-theme/components";
import material from "../../../../native-base-theme/variables/material";
import LocalData, {FIRST_STEP_TWO} from "../../../libs/LocalData";
import env from "../../../config/env";


class FirstStepRegisterTwo extends Component {

    constructor(props) {
        super(props);

        this.state = {
            txtCountry: 'Argentina',
            txtCountryCode: '54',
            txtCityCode: '',
            txtCityCodeIsInvalid: false,
            txtUrl: '',
            txtUrlIsInvalid: false,
            txtCity: '',
            txtCityIsInvalid: false,
            txtPhoneNumber: '',
            txtPhoneNumerIsInvalid: false,
        };
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
        LocalData.get(FIRST_STEP_TWO).then(data => {
            if (data !== null) {
                this.setState({
                    txtCountry: data.country,
                    txtCountryCode: data.country_code,
                    txtCityCode: data.city_code,
                    txtUrl: data.url,
                    txtCity: data.city,
                    txtPhoneNumber: data.phone_number,
                });
            }
        });
    }

    onFocusEvt = (event, textState) => {
        if (!this.state[textState]) {
            this.setAttribute(textState, ' ');
        }
    };

    onBackPress = () => {
        this.onBack();
        return true;
    };

    onBack = () => {
        Actions.firstStepRegisterOne();
    };

    onNext = () => {
        if (this.state.txtUrl.trim() === '' || this.state.txtCity.trim() === '' ||
            this.state.txtCountry.trim() === '' || this.state.txtCountry.trim() === '' ||
            this.state.txtCityCode.trim() === '' || this.state.txtPhoneNumber.trim() === '') {

            if (this.state.txtUrl.trim() === '') {
                this.setState({txtUrlIsInvalid: true});
            }
            if (this.state.txtCity.trim() === '') {
                this.setState({txtCityIsInvalid: true});
            }
            if (this.state.txtCityCode.trim() === '') {
                this.setState({txtCityCodeIsInvalid: true});
            }
            if (this.state.txtCountry.trim() === '') {
                this.setState({txtCountryIsInvalid: true});
            }
            if (this.state.txtPhoneNumber.trim() === '') {
                this.setState({txtPhoneNumberIsInvalid: true});
            }

            Alert.alert(
                'Error',
                'Por favor, ingrese datos válidos',
                [
                    {text: 'OK', onPress: () => console.log('OK Pressed!')},
                ]
            )
        } else {
            LocalData.save(FIRST_STEP_TWO, {
                url: this.state.txtUrl.trim(),
                city: this.state.txtCity.trim(),
                city_code: this.state.txtCityCode.trim(),
                country: this.state.txtCountry.trim(),
                country_code: this.state.txtCountryCode.trim(),
                phone_number: this.state.txtPhoneNumber.trim(),
            }).then(obj => {
                Actions.secondStepRegisterOne();
            })
        }
    };

    setAttribute = (property, value) => this.setState({[property]: value});

    textInputChange = (event, textState, isInvalid) => {
        const text = event.nativeEvent.text;
        if (text) {
            this.setAttribute(textState, text);
            this.setAttribute(isInvalid, false);
        } else {
            this.setAttribute(textState, text);
            this.setAttribute(isInvalid, true);
        }

    };

    selectCountry = (country) => {
        const obj = env.countries.find(obj => obj.country === country);
        if (obj) {
            this.setState({
                txtCountryCode: obj.code,
                txtCountry: country,
                txtCountryIsInvalid: false
            })
        }
    };

    render() {
        return (
            <StyleProvider style={getTheme(material)}>
                <Container>
                    <Header>
                        <Left>
                            <Button transparent onPress={this.onBack}>
                                <Icon style={{color: '#4d9f4d'}} name='arrow-back'/>
                            </Button>
                        </Left>
                        <Body>
                        <Title>Registro</Title>
                        </Body>
                        <Right />
                    </Header>
                    <Content>
                        <Form>
                            <Item stackedLabel={this.state.txtUrl.length > 0}
                                  floatingLabel={!this.state.txtUrl.length < 0}
                                  style={{marginLeft: 25, marginRight: 25}}
                                  error={this.state.txtUrlIsInvalid}>
                                <Label>Url sitio web</Label>
                                <Input keyboardType="url"
                                       value={this.state.txtUrl}
                                       returnKeyType={'next'}
                                       ref={c => {
                                           this._txtUrl = c;
                                       }}
                                       onSubmitEditing={event => {
                                           this._txtCity._root.focus();
                                       }}
                                       onFocus={event => this.onFocusEvt(event, 'txtUrl') }
                                       onChange={event => this.textInputChange(event, 'txtUrl', 'txtUrlIsInvalid')}/>
                            </Item>
                            <Item stackedLabel={this.state.txtCity.length > 0}
                                  floatingLabel={!this.state.txtCity.length < 0}
                                  style={{marginLeft: 25, marginRight: 25}}
                                  error={this.state.txtCityIsInvalid}>
                                <Label>Ciudad</Label>
                                <Input value={this.state.txtCity}
                                       returnKeyType={'next'}
                                       ref={c => {
                                           this._txtCity = c;
                                       }}
                                       onSubmitEditing={event => {
                                           this._txtCityCode._root.focus();
                                       }}
                                       onFocus={event => this.onFocusEvt(event, 'txtCity') }
                                       onChange={event => this.textInputChange(event, 'txtCity', 'txtCityIsInvalid')}/>
                            </Item>
                            <Item inlineLabel
                                  style={{marginLeft: 25, marginRight: 25}}
                                  error={this.state.txtCountryIsInvalid}>
                                <Label>País</Label>
                                <Picker
                                    style={{flex: 0.4, color: '#b1b1b1'}}
                                    selectedValue={this.state.txtCountry}
                                    onValueChange={this.selectCountry}>
                                    {
                                        env.countries.sort((a, b) => {
                                            if (a.country.toLowerCase() < b.country.toLowerCase()) return -1;
                                            if (a.country.toLowerCase() > b.country.toLowerCase()) return 1;
                                            return 0;
                                        }).map((obj, index) => (
                                            <Picker.Item key={index} label={obj.country} value={obj.country}/>
                                        ))
                                    }
                                </Picker>
                            </Item>
                            <ListItem
                                style={{marginLeft: 25, marginRight: 25}}>
                                <Text style={{color: '#5d5d5d'}}>Agregar número telefónico</Text>
                            </ListItem>
                            <Item inlineLabel
                                  style={{marginLeft: 25, marginRight: 25}}>
                                <Picker
                                    style={{flex: 0.3, color: '#b1b1b1'}}
                                    selectedValue={this.state.txtCountryCode}
                                    onValueChange={(country) => this.setState({txtCountry: country})}>
                                    {env.countries.sort((a, b) => {
                                        if (a.country.toLowerCase() < b.country.toLowerCase()) return -1;
                                        if (a.country.toLowerCase() > b.country.toLowerCase()) return 1;
                                        return 0;
                                    }).map((obj, index) => (
                                        <Picker.Item key={index} label={obj.code} value={obj.code}/>
                                    ))}
                                </Picker>
                                <Item stackedLabel={this.state.txtCityCode.length > 0}
                                      floatingLabel={!this.state.txtCityCode.length < 0}
                                      style={{flex: 0.8, marginTop: 10}}
                                      error={this.state.txtCityCodeIsInvalid}>
                                    <Label>Cod. Area</Label>
                                    <Input keyboardType="phone-pad"
                                           value={this.state.txtCityCode}
                                           returnKeyType={'next'}
                                           ref={c => {
                                               this._txtCityCode = c;
                                           }}
                                           onSubmitEditing={event => {
                                               this._txtPhoneNumber._root.focus();
                                           }}
                                           onFocus={event => this.onFocusEvt(event, 'txtCityCode') }
                                           onChange={event => this.textInputChange(event, 'txtCityCode', 'txtCityCodeIsInvalid')}/>
                                </Item>
                            </Item>
                            <Item stackedLabel={this.state.txtPhoneNumber.length > 0}
                                  floatingLabel={!this.state.txtPhoneNumber.length < 0}
                                  style={{marginLeft: 25, marginRight: 25}}
                                  error={this.state.txtPhoneNumberIsInvalid}>
                                <Label>Teléfono</Label>
                                <Input keyboardType="phone-pad"
                                       value={this.state.txtPhoneNumber}
                                       returnKeyType={'next'}
                                       ref={c => {
                                           this._txtPhoneNumber = c;
                                       }}
                                       onFocus={event => this.onFocusEvt(event, 'txtPhoneNumber') }
                                       onChange={event => this.textInputChange(event, 'txtPhoneNumber', 'txtPhoneNumberIsInvalid')}/>
                            </Item>
                        </Form>
                    </Content>
                    <Footer>
                        <View style={{flex: 1}}/>
                        <Button style={{flex: 3, alignItems: 'center', justifyContent: 'center'}} rounded success
                                onPress={this.onNext}>
                            <Text style={{color: '#fff', fontSize: 14}}>SIGUIENTE</Text>
                        </Button>
                        <View style={{flex: 1}}/>
                    </Footer>
                </Container>
            </StyleProvider>
        )
    }
}

export default FirstStepRegisterTwo;