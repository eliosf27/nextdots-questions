import React, {Component} from "react";
import {StyleSheet, Text} from "react-native";
import ResponsiveImage from "react-native-responsive-image";
import {Actions} from "react-native-router-flux";
import Email from "../../../libs/Email";
import LocalData, {FIRST_STEP_ONE} from "../../../libs/LocalData";

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: '#fff',
        flex: 1,
        margin: 5,
        justifyContent: 'center',

    },
    title: {
        fontSize: 14,
        fontWeight: 'bold',
        marginTop: 300
    },
});


class ThirdStepRegisterOne extends Component {

    componentDidMount = () => {
        LocalData.get(FIRST_STEP_ONE).then(data => {
            Email.send(data.email)
                .then(response => {
                    console.log("response: ", response.json());
                    Actions.thirdStepRegisterTwo({email: data.email});
                })
                .catch(error => console.log("error: ", error));
        });

    };

    render() {
        return (
            <ResponsiveImage style={styles.container}
                             source={require('../../../assets/enviando.png')}
                             resizeMode="center">
                <Text style={styles.title}>Enviando correo...</Text>
            </ResponsiveImage>
        )
    }
}

export default ThirdStepRegisterOne;