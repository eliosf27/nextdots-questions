import React, {Component} from "react";
import ResponsiveImage from "react-native-responsive-image";
import {StyleSheet, Text, View} from "react-native";
const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: '#fff',
        flex: 1,
        flexDirection: 'column',
        margin: 10,
        justifyContent: 'center'
    }
});

class FiveStep extends Component {
    render() {
        return (
            <View style={styles.container}>
                <ResponsiveImage
                    source={{uri: 'http://www.nextdots.com/assets/img/logo-colored.png'}}
                    initWidth="538" initHeight="538"
                    resizeMode="center"
                />
                <Text>Thank you for sharing your time with us</Text>
            </View>
        )
    }
}

export default FiveStep;