import React, {Component} from "react";
import {StyleSheet, View} from "react-native";
import {Actions} from "react-native-router-flux";
import ResponsiveImage from "react-native-responsive-image";

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: '#fff',
        flex: 1,
        flexDirection: 'column',
        margin: 5,
        justifyContent: 'center'
    }
});

class Splash extends Component {
    componentDidMount = () => {
        setTimeout(() => {
            Actions.initial();
        }, 1500);
    };

    render() {
        return (
            <View style={styles.container}>
                <ResponsiveImage source={require('../../../assets/fondo.png')}
                                 initWidth="415" initHeight="700"/>
            </View>
        )
    }
}

export default Splash;