import React, {Component} from "react";
import {Actions} from "react-native-router-flux";
import {BackHandler, StyleSheet, Text, View} from "react-native";
import {
    Body,
    Button,
    CheckBox,
    Container,
    Content,
    Footer,
    Header,
    Icon,
    Left,
    ListItem,
    Right,
    StyleProvider,
    Title
} from "native-base";
import getTheme from "../../../../native-base-theme/components";
import material from "../../../../native-base-theme/variables/material";
import LocalData, {SECOND_STEP_ONE} from "../../../libs/LocalData";

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        height: 50,
        padding: 20
    },
    selected: {
        backgroundColor: '#e6b633',
        borderRadius: 5,
        flex: 0.3,
        margin: 2
    },
    unselected: {
        backgroundColor: '#f2f2f2',
        borderRadius: 5,
        flex: 0.3,
        margin: 2
    },
    fixSized: {
        flex: 1
    },
    nextButton: {
        color: '#fff',
        fontSize: 14
    }
});

class SecondStepRegisterOne extends Component {

    constructor(props) {
        super(props);
        this.state = {
            chkboxAll: false,
            chkboxAndroid: false,
            chkboxIOS: false,
            chkboxJs: false,
            chkboxPHP: false,
        };
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
        LocalData.get(SECOND_STEP_ONE).then(data => {
            if (data !== null) {
                this.setState({
                    chkboxAll: data.all,
                    chkboxAndroid: data.android,
                    chkboxIOS: data.ios,
                    chkboxJs: data.js,
                    chkboxPHP: data.php,
                });
            }
        });
    }

    onBackPress = () => {
        this.onBack();
        return true;
    };

    onBack = () => {
        Actions.firstStepRegisterTwo();
    };

    onNext = () => {
        LocalData.save(SECOND_STEP_ONE, {
            android: this.state.chkboxAndroid,
            ios: this.state.chkboxIOS,
            js: this.state.chkboxJs,
            php: this.state.chkboxPHP,
            all: this.state.chkboxAll,
        }).then(obj => {
            console.log("obj ", obj);
            Actions.secondStepRegisterTwo();
        });
    };

    setAttribute = (property, value) => {
        this.setState({[property]: value});
    };

    chkboxCheckChkboxAll = () => {
        let checkValue;
        checkValue = !this.state.chkboxAll;
        this.setAttribute('chkboxAll', checkValue);
        this.setAttribute('chkboxAndroid', checkValue);
        this.setAttribute('chkboxIOS', checkValue);
        this.setAttribute('chkboxJs', checkValue);
        this.setAttribute('chkboxPHP', checkValue);
    };
    chkboxCheckChkboxAndroid = () => this.setAttribute('chkboxAndroid', !this.state.chkboxAndroid);
    chkboxCheckChkboxIOS = () => this.setAttribute('chkboxIOS', !this.state.chkboxIOS);
    chkboxCheckChkboxJs = () => this.setAttribute('chkboxJs', !this.state.chkboxJs);
    chkboxCheckChkboxPHP = () => this.setAttribute('chkboxPHP', !this.state.chkboxPHP);

    render() {
        return (
            <StyleProvider style={getTheme(material)}>
                <Container>
                    <Header>
                        <Left>
                            <Button transparent onPress={this.onBack}>
                                <Icon style={{color: '#e6b633'}} name='arrow-back'/>
                            </Button>
                        </Left>
                        <Body>
                        <Title>Intereses</Title>
                        </Body>
                        <Right />
                    </Header>
                    <Content>
                        <View style={styles.container}>
                            <View style={styles.selected}/>
                            <View style={styles.unselected}/>
                            <View style={styles.unselected}/>
                        </View>
                        <View>
                            <ListItem onPress={this.chkboxCheckChkboxAll}>
                                <CheckBox checked={this.state.chkboxAll}
                                          onPress={this.chkboxCheckChkboxAll}/>
                                <Text style={{marginLeft: 20}}> Staff Augmentation Services</Text>
                            </ListItem>
                            <View style={{marginLeft: 30}}>
                                <ListItem onPress={this.chkboxCheckChkboxAndroid}>
                                    <CheckBox checked={this.state.chkboxAndroid}
                                              onPress={this.chkboxCheckChkboxAndroid}/>
                                    <Text style={{marginLeft: 20}}> Android</Text>
                                </ListItem>
                                <ListItem onPress={this.chkboxCheckChkboxIOS}>
                                    <CheckBox checked={this.state.chkboxIOS}
                                              onPress={this.chkboxCheckChkboxIOS}/>
                                    <Text style={{marginLeft: 20}}> iOS</Text>
                                </ListItem>
                                <ListItem onPress={this.chkboxCheckChkboxJs}>
                                    <CheckBox checked={this.state.chkboxJs}
                                              onPress={this.chkboxCheckChkboxJs}/>
                                    <Text style={{marginLeft: 20}}> Javascript</Text>
                                </ListItem>
                                <ListItem onPress={this.chkboxCheckChkboxPHP}>
                                    <CheckBox checked={this.state.chkboxPHP}
                                              onPress={this.chkboxCheckChkboxPHP}/>
                                    <Text style={{marginLeft: 20}}> PHP</Text>
                                </ListItem>
                            </View>
                        </View>
                    </Content>
                    <Footer>
                        <View style={styles.fixSized}/>
                        <Button style={{flex: 3, alignItems: 'center', justifyContent: 'center'}} rounded warning
                                onPress={this.onNext}>
                            <Text style={styles.nextButton}>SIGUIENTE</Text>
                        </Button>
                        <View style={styles.fixSized}/>
                    </Footer>
                </Container>
            </StyleProvider>
        )
    }
}

export default SecondStepRegisterOne;