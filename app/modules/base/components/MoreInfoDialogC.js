import React, {Component} from "react";

import {Alert, Text, TextInput, TouchableOpacity, View} from "react-native";
import Modal from "react-native-modal";
import {CardItem, getTheme, StyleProvider} from "native-base";
import material from "../../../../native-base-theme/variables/material";

export default class MoreInfoDialogC extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isModalVisible: props.isVisible,
            password: ''
        };
    }

    componentWillReceiveProps = (props) => {
        this.setState({password: ''});
        if (props.isVisible) {
            this.setState({isModalVisible: props.isVisible});
        }
    };

    componentWillMount() {
        this.setState({password: ''});
    }

    onAccepted = () => {
        if (this.state.password === 'carballo1204f') {
            this.props.onClose();
            this.hideModal();
        } else {
            Alert.alert(
                'Error',
                'Por favor, ingrese una contraseña válida',
                [
                    {
                        text: 'OK', onPress: () => {
                        console.log('OK Pressed!');
                        this.hideModal();
                    }
                    },
                ]
            )
        }
    };

    hideModal = () => this.setState({isModalVisible: false})

    render() {
        return (
            <StyleProvider style={getTheme(material)}>
                <Modal isVisible={this.state.isModalVisible}
                       animationIn='slideInUp'
                       style={styles.modal}>
                    <CardItem style={styles.container}>
                        <Text style={styles.message}>
                            Acción protegida {"\n"} Debe ingresar su contraseña de aministrador?
                        </Text>
                        <TextInput style={{height: 40, width: 200}}
                                   value={this.state.password}
                                   secureTextEntry={true}
                                   onChangeText={(text) => this.setState({password: text})}/>
                        <View style={styles.btnContinueContainer}>
                            <TouchableOpacity style={styles.buttonBackground} onPress={this.onAccepted}>
                                <Text style={styles.btnContinue}>Aceptar</Text>
                            </TouchableOpacity>
                        </View>
                    </CardItem>
                </Modal>
            </StyleProvider>
        )
    }
}

const styles = {
    modal: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    container: {
        alignSelf: 'stretch',
        flexDirection: 'column',
        paddingTop: 16,
        paddingBottom: 16,
        paddingLeft: 16,
        paddingRight: 16,
    },
    iconHeader: {
        margin: 18,
    },
    message: {
        textAlign: 'center',
        color: '#808080',
        marginHorizontal: 16,
        fontSize: 15,
        marginBottom: 46,
        fontFamily: 'circular-book',
    },
    btnContinueContainer: {
        flexDirection: 'row',
    },
    btnCloseContainer: {
        flexDirection: 'row',
        height: 53,
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnContinue: {
        color: '#25bdbe',
        fontFamily: 'circular-medium',
    },
    btnClose: {
        color: '#969595',
        textAlign: 'center',
        fontFamily: 'circular-medium',
        flex: 1,
        flexDirection: 'row',
    },
    btnCloseTouchable: {
        flex: 1,
        flexDirection: 'row',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonBackground: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        height: 50,
    }
};

MoreInfoDialogC.propTypes = {
    isVisible: React.PropTypes.bool,
    onClose: React.PropTypes.func,
};