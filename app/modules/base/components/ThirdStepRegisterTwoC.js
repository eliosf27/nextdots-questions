import React, {Component} from "react";
import {StyleSheet, Text} from "react-native";
import ResponsiveImage from "react-native-responsive-image";
import {Actions} from "react-native-router-flux";

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: '#fff',
        flex: 1,
        margin: 5,
        justifyContent: 'center',

    },
    title: {
        fontSize: 14,
        fontWeight: 'bold',
        marginTop: 300
    },
    subtitle: {
        color: '#35a0d9',
        fontSize: 14,
        fontWeight: 'bold',
    }
});


class ThirdStepRegisterTwo extends Component {

    componentDidMount = () => {
        setTimeout(() => {
            Actions.fourStepRegister();
        }, 2000);
    };

    render() {
        return (
            <ResponsiveImage style={styles.container}
                             source={require('../../../assets/enviado.png')}
                             resizeMode="center">
                <Text style={styles.title}>Enviando correctamente a:</Text>
                <Text style={styles.subtitle}>{this.props.email}</Text>
            </ResponsiveImage>
        )
    }
}

export default ThirdStepRegisterTwo;