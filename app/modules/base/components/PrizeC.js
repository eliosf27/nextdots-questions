import React, {Component} from "react";
import {Body, Container, Content, List, ListItem, Right, Text, View} from "native-base";
import {connect} from "react-redux";
import {getUsers} from "../../../modules/base/reducers";
import * as Progress from "react-native-progress";
import StringUtils from "../../../libs/StringsUtils";
import ResponsiveImage from "react-native-responsive-image";
import Dash from "../../../libs/Dash";
import {Actions} from "react-native-router-flux";
import {BackHandler} from "react-native";


class PrizeC extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: true,
            users: null
        };
    }

    componentDidMount = () => {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
        this.props.getUsers();
    };

    onBackPress = () => {
        this.onBack();
        return true;
    };

    onBack = () => {
        Actions.main();
    };

    shuffleList = (arrayObject) => {
        let len = 0;
        if (arrayObject.length >= 10) {
            len = 10;
        } else {
            len = arrayObject.length + 1;
        }
        const data = [];
        for (let i = 0; i < 10; i++) {
            let each = arrayObject[Math.floor(Math.random() * arrayObject.length)];
            data.push(each);
        }
        return data;
    }

    componentWillReceiveProps = (props) => {
        if (props.users.success) {
            let data = props.users.data;
            this.setState({users: this.shuffleList(data)});
        }
    };

    render() {
        return (
            <Container>
                <Content>
                    <View style={{
                        marginLeft: 20,
                        flex: 1,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'flex-start'
                    }}>
                        <ResponsiveImage source={require('../../../assets/logo.png')}
                                         initWidth="150" initHeight="100" resizeMode="contain"/>
                        <Dash style={{width: 1, height: 50, marginLeft: 25, flexDirection: 'column'}}/>
                        <Text style={{fontWeight: 'bold', marginLeft: 25, marginTop: 5, fontSize: 28}}>Ganadores </Text>
                    </View>
                    <View>
                        {this.props.users.loading === true ?
                            <View style={{alignItems: 'center', justifyContent: 'center', marginTop: 150}}>
                                <Progress.CircleSnail size={70} color={['#e01560', '#00a048', '#f6b719', '#0099d9']}/>
                            </View>
                            :
                            <View>
                                {!this.props.users.error && this.state.users ?
                                    <List>
                                        {this.state.users.map((obj, index) => (

                                            <ListItem key={index}>
                                                <Body>
                                                <View style={{flexDirection: 'column'}}>
                                                    <Text style={{
                                                        fontWeight: 'bold',
                                                        margin: 5,
                                                        fontSize: 18
                                                    }}>
                                                        {StringUtils.capitalize(obj.user.data.name)} {StringUtils.capitalize(obj.user.data.last_name)}
                                                    </Text>
                                                    <Text style={{margin: 5, fontSize: 14}}>
                                                        {StringUtils.capitalize(obj.user.data.enterprise)}
                                                    </Text>
                                                </View>
                                                </Body>
                                                <Right/>
                                            </ListItem>
                                        ))}
                                    </List>
                                    :
                                    <View style={{alignItems: 'center', justifyContent: 'center', marginTop: 25}}>
                                        <Text style={{fontWeight: 'bold', margin: 5, fontSize: 20}}>
                                            {this.props.users.error}
                                        </Text>
                                    </View>
                                }
                            </View>
                        }
                    </View>
                </Content>
            </Container>
        )
    }
}

const mapStateToProps = state => (
    {
        users: state.users
    }
);

const mapDispatchToProps = dispatch => (
    {
        getUsers: () => {
            dispatch(getUsers());
        }
    }
);

PrizeC.propTypes = {
    getUsers: React.PropTypes.func.isRequired,
    users: React.PropTypes.object.isRequired
};

const Prize = connect(mapStateToProps, mapDispatchToProps)(PrizeC);

export default Prize;