import React, {Component} from "react";
import {Alert, TouchableOpacity} from "react-native";
import {Body, Container, Content, List, ListItem, Right, Text, View} from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";
import ActionButton from "react-native-action-button";
import {Actions} from "react-native-router-flux";
import {connect} from "react-redux";
import {getUsers} from "../../../modules/base/reducers";
import * as Progress from "react-native-progress";
import StringUtils from "../../../libs/StringsUtils";
import ResponsiveImage from "react-native-responsive-image";
import Dash from "../../../libs/Dash";
import FirebaseDB from "../../../libs/FirebaseDB";
import MoreInfoDialogC from "./MoreInfoDialogC";

class MainC extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: true,
            users: null,
            touches: 0,
            isModalVisible: false
        };
    }

    componentDidMount = () => {
        this.props.getUsers();
    };

    componentWillReceiveProps = (props) => {
        if (props.users.success) {
            let data = props.users.data;
            this.setState({users: data});
        }
    };

    onEditItem = (data) => {
        Actions.edit({data: data});
    };

    onNewItem = () => {
        Actions.firstStepRegisterOne();
    };

    onGeneratePrize = () => {
        if (this.state.touches === 3) {
            Actions.prize();
        } else {
            this.setState({touches: this.state.touches + 1});
        }
    };

    onRemoveEvent = () => {
        Alert.alert(
            'Atención',
            '¿Está seguro que desea eliminar el registro?',
            [
                {text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {
                    text: 'Si', onPress: () => {
                    console.log('OK Pressed');
                    this.setModalVisible(true);
                }
                },
            ]
        )
    };

    onRemoveItem = (id) => {
        this.setModalVisible(false);
        let firebaseDB = new FirebaseDB();
        firebaseDB.removeData(id);
        this.props.getUsers();
    }

    setModalVisible(visible) {
        this.setState({isModalVisible: visible});
    }

    render() {
        return (
            <Container>
                <Content>
                    <TouchableOpacity onPress={this.onGeneratePrize}>
                        <View style={{
                            marginLeft: 20,
                            flex: 1,
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'flex-start'
                        }}>

                            <ResponsiveImage source={require('../../../assets/logo.png')}
                                             initWidth="150" initHeight="100" resizeMode="contain"/>
                            <Dash style={{width: 1, height: 50, marginLeft: 25, flexDirection: 'column'}}/>
                            <Text style={{fontWeight: 'bold', marginLeft: 25, marginTop: 5, fontSize: 28}}>Lista</Text>
                        </View>
                    </TouchableOpacity>
                    <View>
                        {this.props.users.loading ?
                            <View style={{alignItems: 'center', justifyContent: 'center', marginTop: 150}}>
                                <Progress.CircleSnail size={70} color={['#e01560', '#00a048', '#f6b719', '#0099d9']}/>
                            </View>
                            :
                            <View>
                                {!this.props.users.error && this.state.users ?
                                    <List>
                                        {this.state.users.map((obj, index) => (

                                            <ListItem key={index}
                                                      onPress={() => this.onEditItem(obj.user)}
                                                      onLongPress={() => this.onRemoveEvent()}>
                                                <Body>
                                                <View style={{flexDirection: 'column'}}>
                                                    <Text style={{
                                                        fontWeight: 'bold',
                                                        margin: 5,
                                                        fontSize: 18
                                                    }}>
                                                        {StringUtils.capitalize(obj.user.data.name)} {StringUtils.capitalize(obj.user.data.last_name)}
                                                    </Text>
                                                    <Text style={{margin: 5, fontSize: 14}}>
                                                        {StringUtils.capitalize(obj.user.data.enterprise)}
                                                    </Text>
                                                </View>
                                                </Body>
                                                <Right>
                                                    <TouchableOpacity
                                                        onPress={() => this.onEditItem(obj)}>
                                                        <Icon name="pencil"/>
                                                    </TouchableOpacity>
                                                </Right>
                                                <MoreInfoDialogC
                                                    isVisible={this.state.isModalVisible}
                                                    onClose={() => this.onRemoveItem(obj.user.id)}/>
                                            </ListItem>
                                        ))}
                                    </List>
                                    :
                                    <View style={{alignItems: 'center', justifyContent: 'center', marginTop: 25}}>
                                        <Text style={{fontWeight: 'bold', margin: 5, fontSize: 20}}>
                                            {this.props.users.error}
                                        </Text>
                                    </View>
                                }
                            </View>
                        }
                    </View>
                </Content>
                <ActionButton buttonColor="#2298d6" onPress={this.onNewItem}
                              spacing={100} icon={<Icon name="plus" size={20} color="#fff" margin={2}/>}>

                </ActionButton>
            </Container>
        )
    }
}

const mapStateToProps = state => (
    {
        users: state.users
    }
);

const mapDispatchToProps = dispatch => (
    {
        getUsers: () => {
            dispatch(getUsers());
        }
    }
);

MainC.propTypes = {
    getUsers: React.PropTypes.func.isRequired,
    users: React.PropTypes.object.isRequired
};

const Main = connect(mapStateToProps, mapDispatchToProps)(MainC);

export default Main;