import React, {Component} from "react";
import {Actions} from "react-native-router-flux";
import {BackHandler, StyleSheet, Text, View} from "react-native";
import {
    Body,
    Button,
    CheckBox,
    Container,
    Content,
    Footer,
    Header,
    Icon,
    Left,
    ListItem,
    Right,
    StyleProvider,
    Title
} from "native-base";
import getTheme from "../../../../native-base-theme/components";
import material from "../../../../native-base-theme/variables/material";
import LocalData, {
    FIRST_STEP_ONE,
    FIRST_STEP_TWO,
    SECOND_STEP_ONE,
    SECOND_STEP_THREE,
    SECOND_STEP_TWO
} from "../../../libs/LocalData";
import FirebaseDB from "../../../libs/FirebaseDB";

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        height: 50,
        padding: 20
    },
    selected: {
        backgroundColor: '#e6b633',
        borderRadius: 5,
        flex: 0.3,
        margin: 2
    },
    unselected: {
        backgroundColor: '#f2f2f2',
        borderRadius: 5,
        flex: 0.3,
        margin: 2
    },
    fixSized: {
        flex: 1
    },
    nextButton: {
        color: '#fff',
        fontSize: 14
    }
});

class SecondStepRegisterTwo extends Component {

    constructor(props) {
        super(props);
        this.state = {
            chkboxAll: false,
            chkboxPartnership: false,
            chkboxJobs: false,
            chkboxInvestments: false,
            chkboxGeneralInquiresOthers: false,
        };
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
        LocalData.get(SECOND_STEP_THREE).then(data => {
            if (data !== null) {
                this.setState({
                    chkboxAll: data.all,
                    chkboxPartnership: data.partnership,
                    chkboxJobs: data.jobs,
                    chkboxInvestments: data.investments,
                    chkboxGeneralInquiresOthers: data.general_inquires_others,
                });
            }
        });
    }

    onBackPress = () => {
        this.onBack();
        return true;
    };

    onBack = () => {
        Actions.secondStepRegisterTwo();
    };

    onNext = () => {
        LocalData.save(SECOND_STEP_THREE, {
            partnership: this.state.chkboxPartnership,
            jobs: this.state.chkboxJobs,
            investments: this.state.chkboxInvestments,
            general_inquires_others: this.state.chkboxGeneralInquiresOthers,
            all: this.state.chkboxAll,
        });

        let objData = {};
        let userdata = {};
        LocalData.get(FIRST_STEP_ONE).then(data => {
            userdata = Object.assign(userdata, data);
            LocalData.get(FIRST_STEP_TWO).then(data => {
                userdata = Object.assign(userdata, data);
                objData["data"] = userdata;
                LocalData.get(SECOND_STEP_ONE).then(data => {
                    objData["staff_augmentation_services"] = data;
                    LocalData.get(SECOND_STEP_TWO).then(data => {
                        objData["outsourcing"] = data;
                        LocalData.get(SECOND_STEP_THREE).then(data => {
                            objData["general_inquires"] = data;

                            let firebaseDB = new FirebaseDB();
                            firebaseDB.pushData(objData);

                            Actions.fourStepRegister();
                        });
                    });
                });
            });
        });
    };

    setAttribute = (property, value) => {
        this.setState({[property]: value});
    };

    chkboxCheckChkboxAll = () => {
        let checkValue;
        checkValue = !this.state.chkboxAll;
        this.setAttribute('chkboxAll', checkValue);
        this.setAttribute('chkboxPartnership', checkValue);
        this.setAttribute('chkboxJobs', checkValue);
        this.setAttribute('chkboxInvestments', checkValue);
        this.setAttribute('chkboxGeneralInquiresOthers', checkValue);
    };
    chkboxCheckChkboxPartnership = () => this.setAttribute('chkboxPartnership', !this.state.chkboxPartnership);
    chkboxCheckChkboxJobs = () => this.setAttribute('chkboxJobs', !this.state.chkboxJobs);
    chkboxCheckChkboxInvestments = () => this.setAttribute('chkboxInvestments', !this.state.chkboxInvestments);
    chkboxCheckChkboxGeneralInquiresOthers = () => this.setAttribute('chkboxGeneralInquiresOthers', !this.state.chkboxGeneralInquiresOthers);

    render() {
        return (
            <StyleProvider style={getTheme(material)}>
                <Container>
                    <Header>
                        <Left>
                            <Button transparent onPress={this.onBack}>
                                <Icon style={{color: '#e6b633'}} name='arrow-back'/>
                            </Button>
                        </Left>
                        <Body>
                        <Title>Intereses</Title>
                        </Body>
                        <Right />
                    </Header>
                    <Content>
                        <View style={styles.container}>
                            <View style={styles.selected}/>
                            <View style={styles.selected}/>
                            <View style={styles.selected}/>
                        </View>
                        <View>
                            <ListItem onPress={this.chkboxCheckChkboxAll}>
                                <CheckBox checked={this.state.chkboxAll}
                                          onPress={this.chkboxCheckChkboxAll}/>
                                <Text style={{marginLeft: 20}}> General Inquires</Text>
                            </ListItem>
                            <View style={{marginLeft: 30}}>
                                <ListItem onPress={this.chkboxCheckChkboxPartnership}>
                                    <CheckBox checked={this.state.chkboxPartnership}
                                              onPress={this.chkboxCheckChkboxPartnership}/>
                                    <Text style={{marginLeft: 20}}> Partnership</Text>
                                </ListItem>
                                <ListItem onPress={this.chkboxCheckChkboxJobs}>
                                    <CheckBox checked={this.state.chkboxJobs}
                                              onPress={this.chkboxCheckChkboxJobs}/>
                                    <Text style={{marginLeft: 20}}> Jobs</Text>
                                </ListItem>
                                <ListItem onPress={this.chkboxCheckChkboxInvestments}>
                                    <CheckBox checked={this.state.chkboxInvestments}
                                              onPress={this.chkboxCheckChkboxInvestments}/>
                                    <Text style={{marginLeft: 20}}> Investments</Text>
                                </ListItem>
                                <ListItem onPress={this.chkboxCheckChkboxGeneralInquiresOthers}>
                                    <CheckBox checked={this.state.chkboxGeneralInquiresOthers}
                                              onPress={this.chkboxCheckChkboxGeneralInquiresOthers}/>
                                    <Text style={{marginLeft: 20}}> Others</Text>
                                </ListItem>
                            </View>
                        </View>
                    </Content>
                    <Footer>
                        <View style={styles.fixSized}/>
                        <Button style={{flex: 3, alignItems: 'center', justifyContent: 'center'}} rounded warning
                                onPress={this.onNext}>
                            <Text style={styles.nextButton}>COMPLETAR</Text>
                        </Button>
                        <View style={styles.fixSized}/>
                    </Footer>
                </Container>
            </StyleProvider>
        )
    }
}

export default SecondStepRegisterTwo;