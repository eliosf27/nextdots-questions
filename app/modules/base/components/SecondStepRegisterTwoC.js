import React, {Component} from "react";
import {Actions} from "react-native-router-flux";
import {BackHandler, StyleSheet, Text, View} from "react-native";
import {
    Body,
    Button,
    CheckBox,
    Container,
    Content,
    Footer,
    Header,
    Icon,
    Left,
    ListItem,
    Right,
    StyleProvider,
    Title
} from "native-base";
import getTheme from "../../../../native-base-theme/components";
import material from "../../../../native-base-theme/variables/material";
import LocalData, {SECOND_STEP_TWO} from "../../../libs/LocalData";

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        height: 50,
        padding: 20
    },
    selected: {
        backgroundColor: '#e6b633',
        borderRadius: 5,
        flex: 0.3,
        margin: 2
    },
    unselected: {
        backgroundColor: '#f2f2f2',
        borderRadius: 5,
        flex: 0.3,
        margin: 2
    },
    fixSized: {
        flex: 1
    },
    nextButton: {
        color: '#fff',
        fontSize: 14
    }
});

class SecondStepRegisterThree extends Component {

    constructor(props) {
        super(props);
        this.state = {
            chkboxAll: false,
            chkboxQualityEngineering: false,
            chkboxUXConsulting: false,
            chkboxMobileProductDevelopment: false,
            chkboxSoftwareMaintenance: false,
            chkboxUXDesign: false,
            chkboxOutsourcingOthers: false,
        };
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
        LocalData.get(SECOND_STEP_TWO).then(data => {
            if (data !== null) {
                this.setState({
                    chkboxAll: data.all,
                    chkboxQualityEngineering: data.quality_engineering,
                    chkboxUXConsulting: data.ux_consulting,
                    chkboxMobileProductDevelopment: data.mobile_product_development,
                    chkboxSoftwareMaintenance: data.software_maintenance,
                    chkboxUXDesign: data.ux_design,
                    chkboxOutsourcingOthers: data.outsourcing_others,
                });
            }
        });
    }

    onBackPress = () => {
        this.onBack();
        return true;
    };

    onBack = () => {
        Actions.secondStepRegisterOne();
    };

    onNext = () => {
        LocalData.save(SECOND_STEP_TWO, {
            quality_engineering: this.state.chkboxQualityEngineering,
            ux_consulting: this.state.chkboxUXConsulting,
            mobile_product_development: this.state.chkboxMobileProductDevelopment,
            software_maintenance: this.state.chkboxSoftwareMaintenance,
            ux_design: this.state.chkboxUXDesign,
            outsourcing_others: this.state.chkboxOutsourcingOthers,
            all: this.state.chkboxAll,
        }).then(obj => {
            console.log("obj ", obj);
            Actions.secondStepRegisterThree();
        });
    };

    setAttribute = (property, value) => {
        this.setState({[property]: value});
    };

    chkboxCheckChkboxAll = () => {
        let checkValue;
        checkValue = !this.state.chkboxAll;
        this.setAttribute('chkboxAll', checkValue);
        this.setAttribute('chkboxQualityEngineering', checkValue);
        this.setAttribute('chkboxUXConsulting', checkValue);
        this.setAttribute('chkboxMobileProductDevelopment', checkValue);
        this.setAttribute('chkboxSoftwareMaintenance', checkValue);
        this.setAttribute('chkboxUXDesign', checkValue);
        this.setAttribute('chkboxOutsourcingOthers', checkValue);
    };
    chkboxCheckChkboxQualityEngineering = () => this.setAttribute('chkboxQualityEngineering', !this.state.chkboxQualityEngineering);
    chkboxCheckChkboxUXConsulting = () => this.setAttribute('chkboxUXConsulting', !this.state.chkboxUXConsulting);
    chkboxCheckChkboxMobileProductDevelopment = () => this.setAttribute('chkboxMobileProductDevelopment', !this.state.chkboxMobileProductDevelopment);
    chkboxCheckChkboxSoftwareMaintenance = () => this.setAttribute('chkboxSoftwareMaintenance', !this.state.chkboxSoftwareMaintenance);
    chkboxCheckChkboxUXDesign = () => this.setAttribute('chkboxUXDesign', !this.state.chkboxUXDesign);
    chkboxCheckChkboxOutsourcingOthers = () => this.setAttribute('chkboxOutsourcingOthers', !this.state.chkboxOutsourcingOthers);

    render() {
        return (
            <StyleProvider style={getTheme(material)}>
                <Container>
                    <Header>
                        <Left>
                            <Button transparent onPress={this.onBack}>
                                <Icon style={{color: '#e6b633'}} name='arrow-back'/>
                            </Button>
                        </Left>
                        <Body>
                        <Title>Intereses</Title>
                        </Body>
                        <Right />
                    </Header>
                    <Content>
                        <View style={styles.container}>
                            <View style={styles.selected}/>
                            <View style={styles.selected}/>
                            <View style={styles.unselected}/>
                        </View>
                        <View>
                            <ListItem onPress={this.chkboxCheckChkboxAll}>
                                <CheckBox checked={this.state.chkboxAll}
                                          onPress={this.chkboxCheckChkboxAll}/>
                                <Text style={{marginLeft: 20}}> Outsourcing</Text>
                            </ListItem>
                            <View style={{marginLeft: 30}}>
                                <ListItem onPress={this.chkboxCheckChkboxQualityEngineering}>
                                    <CheckBox checked={this.state.chkboxQualityEngineering}
                                              onPress={this.chkboxCheckChkboxQualityEngineering}/>
                                    <Text style={{marginLeft: 20}}> Quality Engineering</Text>
                                </ListItem>
                                <ListItem onPress={this.chkboxCheckChkboxUXConsulting}>
                                    <CheckBox checked={this.state.chkboxUXConsulting}
                                              onPress={this.chkboxCheckChkboxUXConsulting}/>
                                    <Text style={{marginLeft: 20}}> UX Consulting</Text>
                                </ListItem>
                                <ListItem onPress={this.chkboxCheckChkboxMobileProductDevelopment}>
                                    <CheckBox checked={this.state.chkboxMobileProductDevelopment}
                                              onPress={this.chkboxCheckChkboxMobileProductDevelopment}/>
                                    <Text style={{marginLeft: 20}}> Mobile Product Development</Text>
                                </ListItem>
                                <ListItem onPress={this.chkboxCheckChkboxSoftwareMaintenance}>
                                    <CheckBox checked={this.state.chkboxSoftwareMaintenance}
                                              onPress={this.chkboxCheckChkboxSoftwareMaintenance}/>
                                    <Text style={{marginLeft: 20}}> Software Maintenance</Text>
                                </ListItem>
                                <ListItem onPress={this.chkboxCheckChkboxUXDesign}>
                                    <CheckBox checked={this.state.chkboxUXDesign}
                                              onPress={this.chkboxCheckChkboxUXDesign}/>
                                    <Text style={{marginLeft: 20}}> UI / UX Design</Text>
                                </ListItem>
                                <ListItem onPress={this.chkboxCheckChkboxOutsourcingOthers}>
                                    <CheckBox checked={this.state.chkboxOutsourcingOthers}
                                              onPress={this.chkboxCheckChkboxOutsourcingOthers}/>
                                    <Text style={{marginLeft: 20}}> Others</Text>
                                </ListItem>
                            </View>
                        </View>
                    </Content>
                    <Footer>
                        <View style={styles.fixSized}/>
                        <Button style={{flex: 3, alignItems: 'center', justifyContent: 'center'}} rounded warning
                                onPress={this.onNext}>
                            <Text style={styles.nextButton}>SIGUIENTE</Text>
                        </Button>
                        <View style={styles.fixSized}/>
                    </Footer>
                </Container>
            </StyleProvider>
        )
    }
}

export default SecondStepRegisterThree;