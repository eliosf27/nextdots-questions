import React, {Component} from "react";

import {View, TouchableOpacity, Text, Image} from "react-native";
import Modal from "react-native-modal";
import {CardItem, getTheme, StyleProvider} from "native-base";
import material from "../../../../native-base-theme/variables/material";

export default class PhotoPreviewDialogC extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isModalVisible: props.isVisible,
            base64: null
        };
    }

    componentWillReceiveProps = (props) => {
        this.setState({password: ''});
        if (props.isVisible) {
            this.setState({isModalVisible: props.isVisible});
        }
    };

    hideModal = () => this.setState({isModalVisible: false})

    render() {
        return (
            <StyleProvider style={getTheme(material)}>
                <Modal isVisible={this.state.isModalVisible}
                       animationIn='slideInUp'
                       style={styles.modal}>
                    <CardItem style={styles.container}>
                        {this.props.base64 &&
                        <View style={{
                            alignItems: 'center',
                            justifyContent: 'center',
                            margin: 20,
                            borderRadius: 10,
                        }}>
                            <Image source={{uri: this.props.base64}} initWidth="65" initHeight="45"
                                   resizeMode={'center'} style={{
                                width: 300,
                                height: 300
                            }}/>
                        </View>
                        }
                        <View style={styles.btnContinueContainer}>
                            <TouchableOpacity style={styles.buttonBackground}
                                              onPress={this.hideModal}>
                                <Text style={styles.btnContinue}>Cerrar</Text>
                            </TouchableOpacity>
                        </View>
                    </CardItem>
                </Modal>
            </StyleProvider>
        )
    }
}

const styles = {
    modal: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    container: {
        alignSelf: 'stretch',
        flexDirection: 'column',
        paddingTop: 16,
        paddingBottom: 16,
        paddingLeft: 16,
        paddingRight: 16,
    },
    iconHeader: {
        margin: 18,
    },
    message: {
        textAlign: 'center',
        color: '#808080',
        marginHorizontal: 16,
        fontSize: 15,
        marginBottom: 46,
        fontFamily: 'circular-book',
    },
    btnContinueContainer: {
        flexDirection: 'row',
    },
    btnCloseContainer: {
        flexDirection: 'row',
        height: 53,
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnContinue: {
        color: '#25bdbe',
        fontFamily: 'circular-medium',
    },
    btnClose: {
        color: '#969595',
        textAlign: 'center',
        fontFamily: 'circular-medium',
        flex: 1,
        flexDirection: 'row',
    },
    btnCloseTouchable: {
        flex: 1,
        flexDirection: 'row',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonBackground: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        height: 50,
    }
};

PhotoPreviewDialogC.propTypes = {
    isVisible: React.PropTypes.bool,
    base64: React.PropTypes.string,
};