import React, {Component} from "react";
import {StyleSheet, Text, View} from "react-native";
import {Button} from "native-base";
import {Actions} from "react-native-router-flux";
import ResponsiveImage from "react-native-responsive-image";

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center'
    },
    containerButton: {
        position: 'absolute',
        bottom: 85,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        flex: 1
    },
    imageBackground: {
        padding: 14,
    },
    title: {
        backgroundColor: 'transparent',
        fontSize: 14,
        fontWeight: 'bold',
        marginTop: 100
    },
    subtitle: {
        backgroundColor: 'transparent',
        fontSize: 14,
        marginTop: 5
    },
    buttonText: {
        color: 'white'
    }

});

class InitialScreen extends Component {

    onNext = () => {
        Actions.main();
    };

    render() {
        return (
            <View style={styles.container}>
                <ResponsiveImage source={require('../../../assets/fondo_initial_screen.png')}
                                 initWidth="415" initHeight="750">
                    <View style={styles.container}>
                        <Text style={styles.title}>BIENVENIDO A NEXTDOTS</Text>
                        <Text style={styles.subtitle}>Carga tus datos de una manera distinta</Text>
                        <View style={styles.containerButton}>
                            <Button style={{
                                flex: 1, alignItems: 'center',
                                justifyContent: 'center', marginLeft: 70, marginRight: 70
                            }}
                                    rounded warning onPress={this.onNext}>
                                <Text style={styles.buttonText}>EMPEZAR</Text>
                            </Button>
                        </View>
                    </View>
                </ResponsiveImage>
            </View>
        )
    }
}

export default InitialScreen;