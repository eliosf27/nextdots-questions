import React, {Component} from "react";
import {Actions} from "react-native-router-flux";
import {
    Body,
    Button,
    Container,
    Content,
    Form,
    Header,
    Icon,
    Input,
    Item,
    Label,
    Left,
    ListItem,
    Picker,
    Right,
    StyleProvider,
    Text,
    Title
} from "native-base";
import {Alert, BackHandler, Image, TouchableOpacity, View} from "react-native";
import getTheme from "../../../../native-base-theme/components";
import material from "../../../../native-base-theme/variables/material";
import env from "../../../config/env";
import FirebaseDB from "../../../libs/FirebaseDB";
import StringUtils from "../../../libs/StringsUtils";
import RNFS from "react-native-fs";
import ImagePicker from "react-native-image-picker";
import PhotoPreviewDialogC from "./PhotoPreviewDialogC";


class EditC extends Component {

    constructor(props) {
        super(props);

        this.state = {
            userID: null,
            txtCityCode: '',
            txtCountryCode: '',
            txtName: '',
            txtNameIsInvalid: false,
            txtLastName: '',
            txtLastNameIsInvalid: false,
            txtEmail: '',
            txtEmailIsInvalid: false,
            txtEnterprise: '',
            txtEnterpriseIsInvalid: false,
            txtPosition: '',
            txtPositionIsInvalid: false,
            txtUrl: '',
            txtUrlIsInvalid: false,
            txtCity: '',
            txtCityIsInvalid: false,
            txtCountry: '',
            txtCountryIsInvalid: false,
            txtPhoneNumber: '',
            txtPhoneNumerIsInvalid: false,
            imagePath: RNFS.PicturesDirectoryPath + 'NextDots.jpg',
            imageBase64: null,
            isModalVisible: false
        };
    }

    componentDidMount = () => {
        if (this._txtName) {
            this._txtName._root.focus();
        }

        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
        if (this.props.data) {
            let data = this.props.data.data;
            let file = null;
            if (this.props.data.image) {
                file = Object.values(this.props.data.image)[0]['file'];
            }
            if (file) {
                this.setState({imageBase64: file})
            }

            this.setState({
                userID: this.props.data.id,
                txtName: data.name,
                txtLastName: data.last_name,
                txtEmail: data.email,
                txtEnterprise: data.enterprise,
                txtPosition: data.position,
                txtUrl: data.url,
                txtCity: data.city,
                txtCityCode: data.city_code,
                txtCountry: data.country,
                txtCountryCode: data.country_code,
                txtPhoneNumber: data.phone_number,
            });
        }
    };

    onFocusEvt = (event, textState) => {
        if (!this.state[textState]) {
            this.setAttribute(textState, ' ');
        }
    };

    onBackPress = () => {
        this.onBack();
        return true;
    };

    onBack = () => {
        Actions.main();
    };

    onEdit = () => {
        if (this.state.txtName === '' || this.state.txtLastName === '' || this.state.txtEmail === '' ||
            this.state.txtEnterprise === '' || this.state.txtPosition === '' || this.state.txtUrl === '' ||
            this.state.txtCity === '' || this.state.txtCountry === '' || this.state.txtCountry === '' ||
            this.state.txtCityCode === '' || this.state.txtPhoneNumber === '') {

            if (this.state.txtName === '') {
                this.setState({txtNameIsInvalid: true});
            }
            if (this.state.txtLastName === '') {
                this.setState({txtLastNameIsInvalid: true});
            }
            if (this.state.txtEmail === '') {
                this.setState({txtEmailIsInvalid: true});
            }
            if (this.state.txtEnterprise === '') {
                this.setState({txtEmailIsInvalid: true});
            }
            if (this.state.txtPosition === '') {
                this.setState({txtPositionIsInvalid: true});
            }
            if (this.state.txtUrl === '') {
                this.setState({txtUrlIsInvalid: true});
            }
            if (this.state.txtCity === '') {
                this.setState({txtCityIsInvalid: true});
            }
            if (this.state.txtCityCode === '') {
                this.setState({txtCityCodeIsInvalid: true});
            }
            if (this.state.txtCountry === '') {
                this.setState({txtCountryIsInvalid: true});
            }
            if (this.state.txtPhoneNumber === '') {
                this.setState({txtPhoneNumberIsInvalid: true});
            }

            Alert.alert(
                'Error',
                'Por favor, ingrese datos válidos',
                [
                    {text: 'OK', onPress: () => console.log('OK Pressed!')},
                ]
            )
        } else {
            if (!StringUtils.validateEmail(this.state.txtEmail)) {
                this.setState({txtEmailIsInvalid: true});
                Alert.alert(
                    'Error',
                    'Por favor, ingrese un correo válido',
                    [
                        {text: 'OK', onPress: () => console.log('OK Pressed!')},
                    ]
                )
            } else {
                let firebaseDB = new FirebaseDB();
                firebaseDB.updateData(this.state.userID, {
                    name: this.state.txtName,
                    last_name: this.state.txtLastName,
                    email: this.state.txtEmail,
                    enterprise: this.state.txtEnterprise,
                    position: this.state.txtPosition,
                    url: this.state.txtUrl,
                    city: this.state.txtCity,
                    city_code: this.state.txtCityCode,
                    country: this.state.txtCountry,
                    country_code: this.state.txtCountryCode,
                    phone_number: this.state.txtPhoneNumber
                });
                Actions.main();
            }
        }
    };

    setAttribute = (property, value) => this.setState({[property]: value});

    textInputChange = (event, textState, isInvalid) => {
        const text = event.nativeEvent.text;
        if (text) {
            this.setAttribute(textState, text);
            this.setAttribute(isInvalid, false);
        } else {
            this.setAttribute(textState, text);
            this.setAttribute(isInvalid, true);
        }
    };

    selectCountry = (country) => {
        const obj = env.countries.find(obj => obj.country === country);
        if (obj) {
            this.setState({
                txtCountryCode: obj.code,
                txtCountry: country,
                txtCountryIsInvalid: false
            })
        }
    };

    addPhoto = () => {
        const options = {
            title: 'Choose an image...',
            takePhotoButtonTitle: 'Camera',
            chooseFromLibraryButtonTitle: 'Gallery',
            cancelButtonTitle: 'Cancel',
            quality: 0.4,
            mediaType: 'photo',
            noData: true
        };

        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
                Alert.alert(
                    'Error',
                    'Ha ocurrido un error al guardar el archivo',
                    [
                        {text: 'OK', onPress: () => console.log('OK Pressed!')},
                    ]
                )
            } else {
                console.log('ImagePicker pick file: ', response);
                RNFS.readFile(response.path, "base64").then(result => {
                    this.setState({imageBase64: result})
                    let firebaseDB = new FirebaseDB();
                    firebaseDB.uploadImage(this.state.userID, result);
                    Alert.alert(
                        'Éxito',
                        'Imagen guardada exitosamente',
                        [
                            {text: 'OK', onPress: () => console.log('OK Pressed!')},
                        ]
                    );
                    Actions.edit();
                });
            }
        });
    };

    setModalVisible(visible) {
        this.setState({isModalVisible: visible});
    }

    render() {
        const image = {base64Icon: 'data:image/png;base64,' + this.state.imageBase64}
        return (
            <StyleProvider style={getTheme(material)}>
                <Container>
                    <Header>
                        <Left>
                            <Button transparent onPress={this.onBack}>
                                <Icon style={{color: '#b9004c'}} name='arrow-back'/>
                            </Button>
                        </Left>
                        <Body>
                        <Title>Editar</Title>
                        </Body>
                        <Right>
                            <Button transparent onPress={this.onEdit}>
                                <Title style={{color: '#b9004c',}}>Guardar</Title>
                            </Button>
                        </Right>
                    </Header>
                    <Content>

                        <Form>
                            {this.state.imageBase64 &&
                            <View style={{
                                alignItems: 'center',
                                justifyContent: 'center',
                                margin: 20,
                                borderRadius: 10,
                            }}>
                                <TouchableOpacity
                                    onPress={() => this.setModalVisible(true)}
                                    onLongPress={this.addPhoto}>
                                    <Image source={{uri: image.base64Icon}} initWidth="65" initHeight="45"
                                           resizeMode={'center'} style={{
                                        width: 100,
                                        height: 100
                                    }}/>
                                </TouchableOpacity>
                            </View>
                            }

                            <PhotoPreviewDialogC
                                isVisible={this.state.isModalVisible}
                                base64={image.base64Icon}/>

                            <Item stackedLabel={this.state.txtName.length > 0}
                                  floatingLabel={!this.state.txtName.length < 0}
                                  style={{marginLeft: 25, marginRight: 25}}
                                  error={this.state.txtNameIsInvalid}>
                                <Label>Nombre</Label>
                                <Input value={this.state.txtName}
                                       returnKeyType={'next'}
                                       ref={c => {
                                           this._txtName = c;
                                       }}
                                       onSubmitEditing={event => {
                                           this._txtLastName._root.focus();
                                       }}
                                       onFocus={event => this.onFocusEvt(event, 'txtName') }
                                       onChange={event => this.textInputChange(event, 'txtName', 'txtNameIsInvalid')}/>
                            </Item>
                            <Item stackedLabel={this.state.txtLastName.length > 0}
                                  floatingLabel={!this.state.txtLastName.length < 0}
                                  style={{marginLeft: 25, marginRight: 25}}
                                  error={this.state.txtLastNameIsInvalid}>
                                <Label>Apellido</Label>
                                <Input value={this.state.txtLastName}
                                       returnKeyType={'next'}
                                       ref={c => {
                                           this._txtLastName = c;
                                       }}
                                       onSubmitEditing={event => {
                                           this._txtEmail._root.focus();
                                       }}
                                       onFocus={event => this.onFocusEvt(event, 'txtLastName') }
                                       onChange={event => this.textInputChange(event, 'txtLastName', 'txtLastNameIsInvalid')}/>
                            </Item>
                            <Item stackedLabel={this.state.txtEmail.length > 0}
                                  floatingLabel={!this.state.txtEmail.length < 0}
                                  style={{marginLeft: 25, marginRight: 25}}
                                  error={this.state.txtEmailIsInvalid}>
                                <Label>Correo electrónico</Label>
                                <Input keyboardType="email-address"
                                       value={this.state.txtEmail}
                                       returnKeyType={'next'}
                                       ref={c => {
                                           this._txtEmail = c;
                                       }}
                                       onSubmitEditing={event => {
                                           this._txtEnterprise._root.focus();
                                       }}
                                       onFocus={event => this.onFocusEvt(event, 'txtEmail') }
                                       onChange={event => this.textInputChange(event, 'txtEmail', 'txtEmailIsInvalid')}/>
                            </Item>
                            <Item stackedLabel={this.state.txtEnterprise.length > 0}
                                  floatingLabel={!this.state.txtEnterprise.length < 0}
                                  style={{marginLeft: 25, marginRight: 25}}
                                  error={this.state.txtEnterpriseIsInvalid}>
                                <Label>Empresa</Label>
                                <Input value={this.state.txtEnterprise}
                                       returnKeyType={'next'}
                                       ref={c => {
                                           this._txtEnterprise = c;
                                       }}
                                       onSubmitEditing={event => {
                                           this._txtPosition._root.focus();
                                       }}
                                       onFocus={event => this.onFocusEvt(event, 'txtEnterprise') }
                                       onChange={event => this.textInputChange(event, 'txtEnterprise', 'txtEnterpriseIsInvalid')}/>
                            </Item>
                            <Item stackedLabel={this.state.txtPosition.length > 0}
                                  floatingLabel={!this.state.txtPosition.length < 0}
                                  style={{marginLeft: 25, marginRight: 25}}
                                  error={this.state.txtPositionIsInvalid}>
                                <Label>Cargo</Label>
                                <Input value={this.state.txtPosition}
                                       returnKeyType={'next'}
                                       ref={c => {
                                           this._txtPosition = c;
                                       }}
                                       onSubmitEditing={event => {
                                           this._txtUrl._root.focus();
                                       }}
                                       onFocus={event => this.onFocusEvt(event, 'txtPosition') }
                                       onChange={event => this.textInputChange(event, 'txtPosition', 'txtPositionIsInvalid')}/>
                            </Item>
                            <Item stackedLabel={this.state.txtUrl.length > 0}
                                  floatingLabel={!this.state.txtUrl.length < 0}
                                  style={{marginLeft: 25, marginRight: 25}}
                                  error={this.state.txtUrlIsInvalid}>
                                <Label>Url sitio web</Label>
                                <Input keyboardType="url"
                                       value={this.state.txtUrl}
                                       returnKeyType={'next'}
                                       ref={c => {
                                           this._txtUrl = c;
                                       }}
                                       onSubmitEditing={event => {
                                           this._txtCity._root.focus();
                                       }}
                                       onFocus={event => this.onFocusEvt(event, 'txtUrl') }
                                       onChange={event => this.textInputChange(event, 'txtUrl', 'txtUrlIsInvalid')}/>
                            </Item>
                            <Item inlineLabel
                                  style={{marginLeft: 25, marginRight: 25}}
                                  error={this.state.txtCountryIsInvalid}>
                                <Label>País</Label>
                                <Picker
                                    style={{flex: 0.4, color: '#b1b1b1'}}
                                    selectedValue={this.state.txtCountry}
                                    onValueChange={this.selectCountry}>
                                    {env.countries.sort((a, b) => {
                                        if (a.country.toLowerCase() < b.country.toLowerCase()) return -1;
                                        if (a.country.toLowerCase() > b.country.toLowerCase()) return 1;
                                        return 0;
                                    }).map((obj, index) => (
                                        <Picker.Item key={index} label={obj.country} value={obj.country}/>
                                    ))}
                                </Picker>
                            </Item>
                            <Item stackedLabel={this.state.txtCity.length > 0}
                                  floatingLabel={!this.state.txtCity.length < 0}
                                  style={{marginLeft: 25, marginRight: 25}}
                                  error={this.state.txtCityIsInvalid}>
                                <Label>Ciudad</Label>
                                <Input value={this.state.txtCity}
                                       returnKeyType={'next'}
                                       ref={c => {
                                           this._txtCity = c;
                                       }}
                                       onSubmitEditing={event => {
                                           this._txtCityCode._root.focus();
                                       }}
                                       onFocus={event => this.onFocusEvt(event, 'txtCity') }
                                       onChange={event => this.textInputChange(event, 'txtCity', 'txtCityIsInvalid')}/>
                            </Item>
                            <ListItem
                                style={{marginLeft: 25, marginRight: 25}}>
                                <Text style={{color: '#5d5d5d'}}>Edita número telefónico</Text>
                            </ListItem>
                            <Item inlineLabel
                                  style={{marginLeft: 25, marginRight: 25}}
                                  error={this.state.txtCityCodeIsInvalid}>
                                <Picker
                                    style={{flex: 0.4, color: '#b1b1b1'}}
                                    selectedValue={this.state.txtCountryCode}
                                    onValueChange={(country) => this.setState({txtCountry: country})}>
                                    {
                                        env.countries.sort((a, b) => {
                                            if (a.country.toLowerCase() < b.country.toLowerCase()) return -1;
                                            if (a.country.toLowerCase() > b.country.toLowerCase()) return 1;
                                            return 0;
                                        }).map((obj, index) => (
                                            <Picker.Item key={index} label={obj.code} value={obj.code}/>
                                        ))
                                    }
                                </Picker>
                                <Item stackedLabel={this.state.txtCityCode.length > 0}
                                      floatingLabel={!this.state.txtCityCode.length < 0}
                                      style={{flex: 0.8, marginTop: 5}}
                                      error={this.state.txtCityCodeIsInvalid}>
                                    <Label>Cod. Area</Label>
                                    <Input keyboardType="phone-pad"
                                           value={this.state.txtCityCode}
                                           returnKeyType={'next'}
                                           ref={c => {
                                               this._txtCityCode = c;
                                           }}
                                           onSubmitEditing={event => {
                                               this._txtPhoneNumber._root.focus();
                                           }}
                                           onFocus={event => this.onFocusEvt(event, 'txtCityCode') }
                                           onChange={event => this.textInputChange(event, 'txtCityCode', 'txtCityCodeIsInvalid')}/>
                                </Item>
                            </Item>
                            <Item stackedLabel={this.state.txtPhoneNumber.length > 0}
                                  floatingLabel={!this.state.txtPhoneNumber.length < 0}
                                  style={{marginBottom: 10, marginLeft: 25, marginRight: 25}}
                                  defaultValue={15}
                                  error={this.state.txtPhoneNumberIsInvalid}>
                                <Label>Teléfono</Label>
                                <Input keyboardType="phone-pad"
                                       value={this.state.txtPhoneNumber}
                                       returnKeyType={'next'}
                                       ref={c => {
                                           this._txtPhoneNumber = c;
                                       }}
                                       onChange={event => this.textInputChange(event, 'txtPhoneNumber', 'txtPhoneNumberIsInvalid')}/>
                            </Item>
                        </Form>
                    </Content>
                </Container>
            </StyleProvider>
        )
    }
}

export default EditC;