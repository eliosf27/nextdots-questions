import React from "react";
import Splash from "./components/SplashC";
import InitialScreen from "./components/InitialScreenC";
import EditC from "./components/EditC";
import FirstStepRegisterOne from "./components/FirstStepRegisterOneC";
import FirstStepRegisterTwo from "./components/FirstStepRegisterTwoC";
import SecondStepRegisterOne from "./components/SecondStepRegisterOneC";
import SecondStepRegisterTwo from "./components/SecondStepRegisterTwoC";
import SecondStepRegisterThree from "./components/SecondStepRegisterThreeC";
import ThirdStepRegisterOne from "./components/ThirdStepRegisterOneC";
import ThirdStepRegisterTwo from "./components/ThirdStepRegisterTwoC";
import FourStepRegister from "./components/FourStepRegisterC";
import FiveStep from "./components/FiveStepC";
import Main from "./components/MainC";
import PrizeC from "./components/PrizeC";
import {ActionConst, Actions, Scene} from "react-native-router-flux";

const scenes = Actions.create(
    <Scene key="root">
        <Scene
            key="splash"
            component={Splash}
            hideNavBar={true}
            initial={true}
            type={ActionConst.REPLACE}
        />
        <Scene
            key="initial"
            component={InitialScreen}
            hideNavBar={true}
            type={ActionConst.REPLACE}
        />
        <Scene
            key="main"
            component={Main}
            hideNavBar={true}
            type={ActionConst.REPLACE}
        />
        <Scene
            key="prize"
            component={PrizeC}
            hideNavBar={true}
            type={ActionConst.REPLACE}
        />
        <Scene
            key="edit"
            component={EditC}
            hideNavBar={true}
            type={ActionConst.REPLACE}
        />
        <Scene
            key="firstStepRegisterOne"
            component={FirstStepRegisterOne}
            hideNavBar={true}
            type={ActionConst.REPLACE}
        />
        <Scene
            key="firstStepRegisterTwo"
            component={FirstStepRegisterTwo}
            hideNavBar={true}
            type={ActionConst.REPLACE}
        />
        <Scene
            key="secondStepRegisterOne"
            component={SecondStepRegisterOne}
            hideNavBar={true}
            type={ActionConst.REPLACE}
        />
        <Scene
            key="secondStepRegisterTwo"
            component={SecondStepRegisterTwo}
            hideNavBar={true}
            type={ActionConst.REPLACE}
        />
        <Scene
            key="secondStepRegisterThree"
            component={SecondStepRegisterThree}
            hideNavBar={true}
            type={ActionConst.REPLACE}
        />
        <Scene
            key="thirdStepRegisterOne"
            component={ThirdStepRegisterOne}
            hideNavBar={true}
            type={ActionConst.REPLACE}
        />
        <Scene
            key="thirdStepRegisterTwo"
            component={ThirdStepRegisterTwo}
            hideNavBar={true}
            type={ActionConst.REPLACE}
        />
        <Scene
            key="fourStepRegister"
            component={FourStepRegister}
            hideNavBar={true}
            type={ActionConst.REPLACE}
        />
        <Scene
            key="five"
            component={FiveStep}
            hideNavBar={true}
            type={ActionConst.REPLACE}
        />
    </Scene>
);

export default scenes;