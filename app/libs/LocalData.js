import simpleStore from "react-native-simple-store";

export const FIRST_STEP_ONE = "FIRST_STEP_ONE";
export const FIRST_STEP_TWO = "FIRST_STEP_TWO";
export const SECOND_STEP_ONE = "SECOND_STEP_ONE";
export const SECOND_STEP_TWO = "SECOND_STEP_TWO";
export const SECOND_STEP_THREE = "SECOND_STEP_THREE";
export const PHOTO_STEP = "PHOTO_STEP";
export const ID = "ID";

export default {
    save: (key, data) => {
        return simpleStore
            .save(key, data)
            .then(() => simpleStore.get(key))
            .catch(error => {
                console.error(error.message);
            });
    },
    get: (key) => {
        return simpleStore.get(key)
            .then(data => {
                return data;
            });
    },
    removeAll: () => {
        const keys = [
            FIRST_STEP_ONE, FIRST_STEP_TWO, SECOND_STEP_ONE,
            SECOND_STEP_TWO, SECOND_STEP_THREE
        ];
        return simpleStore.delete(keys).then(data => {
            return data;
        });
    }
}