import env from "../config/env";

export default {
    send: (email) => {
        return fetch('https://api.sparkpost.com/api/v1/transmissions', {
            method: 'POST',
            headers: {
                'Authorization': env.sparkpostApiKey,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "options": {"sandbox": true},
                "content": {
                    "from": "testing@sparkpostbox.com",
                    "subject": "Oh hey",
                    "text": "Testing SparkPost - the most awesomest email service in the world"
                },
                "recipients": [{"address": email}]
            })
        })
    },
}