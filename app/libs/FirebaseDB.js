import firebase from "firebase";
import env from "../config/env";
import LocalData, {ID} from "../libs/LocalData";

export default class FirebaseDB {
    constructor() {
        if (!firebase.apps.length) {
            firebase.initializeApp(env.firebaseConfig);
        }
    }

    pushData = (data) => {
        firebase.database().ref('users/').push({
            user: data
        }).then((snap) => {
            const key = snap.key;
            console.log("key: ", key);
            LocalData.save(ID, key);
        });
    };

    removeData = (id) => {
        firebase.database().ref('users/' + id).child("user").remove();
    };

    updateData = (id, data) => {
        firebase.database().ref('users/' + id).child("user").child("data").update(data);
    };

    uploadImage = (id, fileContent) => {
        firebase.database().ref('users/' + id).child("user").child("image").push({
            file: fileContent
        });
    };

    getData = () => firebase.database().ref('users');
}