# Expo Internet - React-Native

## Run the project iOS

1. First, clone de Github project:
    `git clone git@bitbucket.org:nextdotsjolivieri/expo-internet.git`
2. Install dependencies:
    `cd expo-internet && npm install`
3. Linking libraries:
    `react-native link`
4. Run the server:
    `react-native start`
5. Run the app or you can launch it from Xcode:
    `react-native run-ios`

## Run the project Android
1. First, clone de Github project:
    `git clone git@bitbucket.org:nextdotsjolivieri/expo-internet.git`
2. Run the following command:
    `cd expo-internet && npm install`
3. Linking libraries:
    `react-native link`
4. Specify the Android SDK path:
    * Create the local.properties file
    `vim android/local.properties`
    * And add the following variable.:
    `sdk.dir=<dir>/Android/adt-bundle-linux-<platform>/sdk`
    * Save the changes:
    `:wq`
5. Run the server:
    `react-native start`
6. Run the app:
    `react-native run-android`

### Build a release
1. Run the following command:
    `cd android && ./gradlew assembleRelease`
2. Find the *.apk signed:
    `expo-internet/android/app/build/outputs/apk/app-release.apk`


Problems:
*A non-recoverable condition has triggered. Watchman needs your help!*

1. Install watchman https://facebook.github.io/watchman/docs/install.html
2. `echo 256 | sudo tee -a /proc/sys/fs/inotify/max_user_instances`
3. `echo 65536 | sudo tee -a /proc/sys/fs/inotify/max_user_watches`
4. `echo 32768 | sudo tee -a /proc/sys/fs/inotify/max_queued_events`
5. `watchman shutdown-server`